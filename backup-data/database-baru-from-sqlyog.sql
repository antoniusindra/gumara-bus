/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.19-0ubuntu0.16.04.1 : Database - gumara_bus
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`gumara_bus` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `gumara_bus`;

/*Table structure for table `bus` */

CREATE TABLE `bus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(255) NOT NULL,
  `merk` varchar(255) DEFAULT NULL,
  `plat_nomor` varchar(255) DEFAULT NULL,
  `warna` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bus_kode_unique` (`kode`),
  UNIQUE KEY `bus_warna_unique` (`warna`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `bus` */

insert  into `bus`(`id`,`kode`,`merk`,`plat_nomor`,`warna`,`created_at`,`updated_at`) values (8,'BUS-01','MERK-01','H 0001 ADG','#ff6384','2018-02-05 08:32:28','2018-02-16 04:14:22'),(9,'BUS-02','MERK-02','H 0002 ADG','#36a2eb','2018-02-05 08:33:02','2018-02-16 04:15:19'),(10,'BUS-03','MERK-03','H 0003 ADG','#4bc0c0','2018-02-05 08:33:24','2018-02-16 04:15:35'),(13,'BUS-04','MERK-04','H 0004 ADG','#9966ff','2018-02-05 08:34:29','2018-02-16 04:15:53'),(14,'BUS-05','MERK-05','H 0005 ADG','#ff9f40','2018-02-05 08:34:58','2018-02-16 04:16:12');

/*Table structure for table `karyawan` */

CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(100) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text,
  `email` varchar(255) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `karyawan_nik_unique` (`nik`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `karyawan` */

insert  into `karyawan`(`id`,`nik`,`nama`,`alamat`,`email`,`no_hp`,`created_at`,`updated_at`) values (2,'NIK-01','Joko Susilo','Jl. Damar No, 342 Bandung','joko@gmail.com','08123234234','2018-02-07 15:27:31','2018-02-07 15:27:31'),(3,'NIK-02','Bambang','Jl. Durian Raya No. 15 Semarang','bambang@gmail.com','09564353533','2018-02-07 15:27:53','2018-02-07 15:27:53'),(4,'NIK-03','Darsono','Jl. Damar No, 342 Bandung','darsono@gmail.com','08943543545','2018-02-07 15:28:16','2018-02-07 15:28:16'),(5,'NIK-04','Sutarji','Jl. Damar No, 342 Bandung','sutarji@gmail.com','0893456353','2018-02-07 15:28:53','2018-02-07 15:28:53'),(6,'NIK-05','Bramanto','Jl. Damar No, 342 Bandung','bramanto@gmail.com','0894534534','2018-02-07 15:29:31','2018-02-07 15:29:31');

/*Table structure for table `pemesanan` */

CREATE TABLE `pemesanan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `no_urut` int(11) NOT NULL,
  `no_pemesanan` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `pj_rombongan` varchar(255) DEFAULT NULL,
  `telp_pj_rombongan` varchar(255) DEFAULT NULL,
  `tanggal_keberangkatan` date NOT NULL,
  `jam_keberangkatan` time NOT NULL,
  `tanggal_kepulangan` date NOT NULL,
  `jam_kepulangan` time NOT NULL,
  `tujuan` varchar(255) NOT NULL,
  `alamat_penjemputan` varchar(255) NOT NULL,
  `jumlah_armada` int(11) DEFAULT NULL,
  `harga_satuan` double DEFAULT NULL,
  `biaya_sewa` double DEFAULT NULL,
  `biaya_tambahan` double DEFAULT NULL,
  `total_biaya` double DEFAULT NULL,
  `uang_muka` double DEFAULT NULL,
  `sisa_pembayaran` double DEFAULT NULL,
  `pembayaran` enum('Tunai','Transfer') DEFAULT NULL,
  `bank` enum('BCA','Mandiri','BRI') DEFAULT NULL,
  `catatan` text,
  `bayar_kekurangan` double DEFAULT NULL,
  `pembayaran_kekurangan` enum('Tunai','Transfer') DEFAULT NULL,
  `bank_kekurangan` enum('BCA','Mandiri','BRI') DEFAULT NULL,
  `user` int(11) DEFAULT NULL,
  `keterangan` text,
  `status` enum('Dipesan Tanpa DP','Dipesan Sudah DP','Lunas') NOT NULL,
  `total_pengeluaran` double DEFAULT NULL,
  `status_pengeluaran` int(11) DEFAULT NULL,
  `zakat_persen` float DEFAULT NULL,
  `solar` double DEFAULT NULL,
  `sopir_persen` float DEFAULT NULL,
  `sopir_rupiah` double DEFAULT NULL,
  `sopir_zakat_rupiah` double DEFAULT NULL,
  `kernet_persen` float DEFAULT NULL,
  `kernet_rupiah` double DEFAULT NULL,
  `kernet_zakat_rupiah` double DEFAULT NULL,
  `pengeluaran_lain` double DEFAULT NULL,
  `pemasukan_zakat_rupiah` double DEFAULT NULL,
  `potongan_kas` double DEFAULT NULL,
  `pemasukan` double DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_pemesanan` (`no_pemesanan`),
  KEY `user` (`user`),
  KEY `input_by` (`input_by`),
  CONSTRAINT `pemesanan_input_by` FOREIGN KEY (`input_by`) REFERENCES `users` (`id`),
  CONSTRAINT `pemesanan_user` FOREIGN KEY (`user`) REFERENCES `karyawan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan` */

insert  into `pemesanan`(`id`,`no_urut`,`no_pemesanan`,`nama`,`telepon`,`pj_rombongan`,`telp_pj_rombongan`,`tanggal_keberangkatan`,`jam_keberangkatan`,`tanggal_kepulangan`,`jam_kepulangan`,`tujuan`,`alamat_penjemputan`,`jumlah_armada`,`harga_satuan`,`biaya_sewa`,`biaya_tambahan`,`total_biaya`,`uang_muka`,`sisa_pembayaran`,`pembayaran`,`bank`,`catatan`,`bayar_kekurangan`,`pembayaran_kekurangan`,`bank_kekurangan`,`user`,`keterangan`,`status`,`total_pengeluaran`,`status_pengeluaran`,`zakat_persen`,`solar`,`sopir_persen`,`sopir_rupiah`,`sopir_zakat_rupiah`,`kernet_persen`,`kernet_rupiah`,`kernet_zakat_rupiah`,`pengeluaran_lain`,`pemasukan_zakat_rupiah`,`potongan_kas`,`pemasukan`,`input_by`,`created_at`,`updated_at`) values (32,1,'000001/04/2018','Pemesan 1','1111111111','PJ Rombongan 1','1111111','2018-04-11','12:00:00','2018-04-14','12:00:00','Tujuan 1','Alamat penjemputan 1',2,1000000,2000000,500000,2500000,1000000,1500000,'Tunai',NULL,NULL,1500000,'Tunai',NULL,NULL,NULL,'Lunas',428125,1,2.5,0,10,243750,6250,5,121875,3125,0,53125,100000,1971875,47,'2018-04-11 08:20:06','2018-04-11 09:17:59'),(33,2,'000002/04/2018','Nama Pemesan','089123232432','123213','08123423432','2018-04-22','12:00:00','2018-04-27','12:00:00','23432423','423432432',2,2000000,4000000,500000,4500000,1000000,3500000,'Tunai',NULL,NULL,3500000,'Tunai',NULL,NULL,NULL,'Lunas',2428125,1,2.5,1500000,10,243750,6250,5,121875,3125,500000,53125,100000,1971875,47,'2018-04-14 12:38:38','2018-04-14 12:39:31');

/*Table structure for table `pemesanan_bus` */

CREATE TABLE `pemesanan_bus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pemesanan_id` int(11) DEFAULT NULL,
  `bus_id` int(11) DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pemesanan_id` (`pemesanan_id`),
  KEY `bus_id` (`bus_id`),
  KEY `input_by` (`input_by`),
  CONSTRAINT `pemesanan_bus_bus_id` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`),
  CONSTRAINT `pemesanan_bus_input_by` FOREIGN KEY (`input_by`) REFERENCES `users` (`id`),
  CONSTRAINT `pemesanan_bus_pemesanan_id` FOREIGN KEY (`pemesanan_id`) REFERENCES `pemesanan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;

/*Data for the table `pemesanan_bus` */

insert  into `pemesanan_bus`(`id`,`pemesanan_id`,`bus_id`,`input_by`,`created_at`,`updated_at`) values (173,32,8,47,'2018-04-11 08:20:06','2018-04-11 08:20:06'),(174,32,9,47,'2018-04-11 08:20:06','2018-04-11 08:20:06'),(175,33,8,47,'2018-04-14 12:38:38','2018-04-14 12:38:38'),(176,33,9,47,'2018-04-14 12:38:38','2018-04-14 12:38:38');

/*Table structure for table `pengeluaran` */

CREATE TABLE `pengeluaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pemesanan_id` int(11) NOT NULL,
  `keperluan` text,
  `biaya` double DEFAULT NULL,
  `input_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pemesanan_id` (`pemesanan_id`),
  KEY `input_by` (`input_by`),
  CONSTRAINT `pengeluaran_input_by` FOREIGN KEY (`input_by`) REFERENCES `users` (`id`),
  CONSTRAINT `pengeluaran_pemesanan_id` FOREIGN KEY (`pemesanan_id`) REFERENCES `pemesanan` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `pengeluaran` */

insert  into `pengeluaran`(`id`,`pemesanan_id`,`keperluan`,`biaya`,`input_by`,`created_at`,`updated_at`) values (1,33,'Makanan',200000,47,'2018-04-14 12:39:31','2018-04-14 12:39:31'),(2,33,'Minuman',300000,47,'2018-04-14 12:39:31','2018-04-14 12:39:31');

/*Table structure for table `presentase` */

CREATE TABLE `presentase` (
  `id` int(11) NOT NULL,
  `zakat` float DEFAULT NULL,
  `gaji_sopir` float DEFAULT NULL,
  `gaji_kernet` float DEFAULT NULL,
  `potongan_kas` double DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `presentase` */

insert  into `presentase`(`id`,`zakat`,`gaji_sopir`,`gaji_kernet`,`potongan_kas`,`created_at`,`updated_at`) values (1,2.5,10,5,100000,'2018-03-29 21:53:14','2018-04-07 03:45:31');

/*Table structure for table `profil` */

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `alamat` text,
  `kota` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `gambar` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `profil` */

insert  into `profil`(`id`,`nama`,`alamat`,`kota`,`email`,`telepon`,`gambar`,`created_at`,`updated_at`) values (1,'PT Gumara Trans Jaya','Jl. Damar No, 342','Bandung','jaya@selalu.com','021-23234234','logo.png','2018-01-23 13:13:40','2018-01-23 07:10:27');

/*Table structure for table `users` */

CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telepon` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`telepon`,`username`,`password`,`remember_token`,`created_at`,`updated_at`) values (47,'Antonius Indra Legowo','antoniusindra773@gmail.com','08898983787','antonius','$2y$10$C44nk0y/NmVqDkzvd5iWL.bGIj/T7yl.U9gOV9tVxh6KKLYcykFb2','cEOYw17BCqg9XzUJDNyHKCyLgQWeU63kWU7pW6Ni1kYsGTEamBYxui30RxQo','2018-01-22 14:11:50','2018-04-07 05:40:54'),(48,'Coba User 1','user1@demo.com','00000000','user1','$2y$10$0HH3aXkkXCA/MPhhfcF6AuLc5W5IB11.cBrXooxYA5prWvDX5J50S',NULL,'2018-01-22 14:12:32','2018-01-22 14:12:32'),(49,'Coba User 2','user2@gmail.com','0000000000','user2','$2y$10$OVCpApAiL4ZyYWvv7mHws.3Obcuw5/F1r7TtFxW.kQe2Ly.h1L.ry',NULL,'2018-01-22 14:12:53','2018-01-22 14:12:53'),(50,'admin','antoniusindra77@gmail.com','081393724486','admin','$2y$10$s.iP0kofhZE1nzKgcpdOruFK3lvuFpV/VV9lflIiIiJEiXspRqfJe',NULL,'2018-04-07 05:41:14','2018-04-07 05:41:14');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
