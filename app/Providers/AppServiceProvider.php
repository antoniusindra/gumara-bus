<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Request;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $varGlobal = array();
        
        if(Request::segment(1) == 'beranda'){
            $varGlobal['beranda'] = 'active';
        }else if(Request::segment(1) == 'bus' || Request::segment(1) == 'karyawan'){
            $varGlobal['master-data'] = 'active';
        }else if(Request::segment(1) == 'user' || Request::segment(1) == 'profil-perusahaan' || Request::segment(1) == 'presentase'){
            $varGlobal['pengaturan'] = 'active';
        }

        View::share('varGlobal', $varGlobal);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
