<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Karyawan;

class KaryawanController extends Controller
{
    private $url;
    private $cari;
    private $jumPerPage = 10;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->cari = Input::get('cari', '');
        $this->url = makeUrl($request->query());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['url'] = $this->url;

        $query = Karyawan::orderBy('id', 'desc');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listKaryawan = $query->paginate($this->jumPerPage);
        (!empty($this->cari))?$listKaryawan->setPath('bus'.$var['url']['cari']):'';

        return view('master-data.karyawan.karyawan-1', compact('var', 'listKaryawan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['method'] =  'create';
        return view('master-data.karyawan.karyawan-2', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Karyawan::create($request->all());
            Session::flash('pesanSukses', 'Data Karyawan Berhasil Disimpan ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Karyawan Gagal Disimpan ...');
            return redirect('karyawan/create')->withInput();
        }

        return redirect('karyawan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $var['method'] = 'show';
        $listKaryawan = Karyawan::find($id);;

        return view('master-data.karyawan.karyawan-2', compact('listKaryawan', 'var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['url'] = $this->url;
        $var['method'] = 'edit';
        $listKaryawan = Karyawan::find($id);;

        return view('master-data.karyawan.karyawan-2', compact('listKaryawan', 'var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var['url'] = $this->url;

        try {
            $tabel = Karyawan::find($id);
            $tabel->update($request->all());

            Session::flash('pesanSukses', 'Data Karyawan Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Karyawan Gagal Diupdate ...');
        }

        return redirect('karyawan'.$var['url']['all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $var['url'] = $this->url;

        try {
            $tabel = Karyawan::find($id);
            $tabel->delete();

            if($request->nomor == '1'){
                $input = $request->query();
                $input['page'] = '1';
                $var['url'] = makeUrl($input);
            }

            Session::flash('pesanSukses', 'Data Karyawan Berhasil Dihapus ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Karyawan Gagal Dihapus ...');
        }

        return redirect('karyawan'.$var['url']['all']);
    }

    public function cekValidasi(Request $request)
    {
        if($request->kolom == 'nik'){
            $kriteria = $request->nik;
        }else if($request->kolom == 'email'){
            $kriteria = $request->email;
        }


        if($request->aksi == 'create'){
            $jumlah = Karyawan::where($request->kolom, $kriteria)->count();

            if ($jumlah != 0) {
                return Response::json(false);
            }else{
                return Response::json(true);
            }
        }else if($request->aksi == 'edit'){
            $jumlah = Karyawan::where($request->kolom, $kriteria)->count();

            if ($jumlah != 0) {
                $jumlah2 = Karyawan::where($request->kolom, $kriteria)->where('id', $request->pk)->count();

                if($jumlah2 == 1){
                   return Response::json(true); 
                }else{
                    return Response::json(false);
                }
            }else{
                return Response::json(true);
            }
        }
    }
}
