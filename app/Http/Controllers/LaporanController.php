<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Pemesanan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Models\ProfilPerusahaan;
use App\Models\Karyawan;
use App\Models\Bus;

class LaporanController extends Controller
{
    function __construct(Request $request)
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['user'] = Karyawan::orderBy('id', 'asc')->pluck('nama', 'id')->all();
        $var['bulan'] = [1=>'Januari', 2=>'Februari', 3=>'Maret', 4=>'April', 5=>'Mei', 6=>'Juni',
                        7=>'Juli', 8=>'Agustus', 9=>'September', 10=>'Oktober', 11=>'Nopember', 12=>'Desember'];

        return view('laporan.form.laporan', compact('var'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function laporan(Request $request)
    {
        if($request->jenis_laporan == 'pemasukan'){
            $pdf = $this->cetakLaporanPengeluaran($request->dari_tanggal, $request->sampai_tanggal);
            return $pdf->stream('laporan-pemasukan.pdf');
        }
    }

    public function cetakLaporanPengeluaran($dari, $sampai)
    {
        $dari_tanggal = Carbon::parse($dari)->format('Y-m-d');
        $sampai_tanggal = Carbon::parse($sampai)->format('Y-m-d');

        $listPemesanan = Pemesanan::orderBy('id', 'asc')->whereBetween('created_at', [$dari_tanggal, $sampai_tanggal])
                ->where('status', 'Lunas')->whereNotNull('pemasukan')->get();
        $profil = ProfilPerusahaan::find(1);

        $pdf = PDF::loadView('laporan.laporan-pemasukan', compact('listPemesanan', 'profil', 'dari', 'sampai'))->setPaper('a4', 'potrait');

        return $pdf;
    }

    public function cetakJadwalBus(Request $request)
    {
        $var['bulan'] = $request->bulan;
        $var['tahun'] = $request->tahun;
        $profil = ProfilPerusahaan::find(1);
        $bus = Bus::orderBy('id', 'asc')->get();

        $pdf = PDF::loadView('laporan.jadwal-bus', compact('var', 'profil', 'bus'))->setPaper('a4', 'landscape');

        return $pdf->stream('jadwal-bus.pdf');
    }
}
