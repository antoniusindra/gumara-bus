<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Pemesanan;
use App\Models\PemesananBus;
use App\Models\Pengeluaran;
use App\Models\Bus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\Presentase;
use PDF;
use App\Models\ProfilPerusahaan;

class PengeluaranController extends Controller
{
    private $url;
    private $cari;
    private $jumPerPage = 10;
    private $jumPerModal = 5;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->cari = Input::get('cari', '');
        $this->url = makeUrl($request->query());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['url'] = $this->url;

        $query = Pemesanan::orderBy('id', 'desc')->where('status_pengeluaran','1');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listPengeluaran = $query->paginate($this->jumPerPage);
        (!empty($this->cari))?$listPengeluaran->setPath('pengeluaran'.$var['url']['cari']):'';

        return view('pengeluaran.pengeluaran-1', compact('var', 'listPengeluaran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['method'] =  'create';
        Session::forget(['biayaPengeluaranLainLain']);

        return view('pengeluaran.pengeluaran-2', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $totalBiaya = 0;

        try {
            if(!empty(session('biayaPengeluaranLainLain'))){
                $deletePengeluaran = Pengeluaran::where('pemesanan_id', $request->id_pemesanan)->delete();
                foreach(session('biayaPengeluaranLainLain') as $item){
                    $totalBiaya += $item['biaya_rincian'];
                    $pengeluaran = new Pengeluaran();
                    $pengeluaran->pemesanan_id = $request->id_pemesanan;
                    $pengeluaran->keperluan = $item['keperluan'];
                    $pengeluaran->biaya = $item['biaya_rincian'];
                    $pengeluaran->input_by = Auth::user()->id;
                    $pengeluaran->save();
                }
            }
            
            
            $totalPengeluaran = $totalBiaya + hilangTitik($request->solar) + hilangTitik($request->sopir_rupiah) + hilangTitik($request->sopir_zakat_rupiah);
            $totalPengeluaran +=  hilangTitik($request->kernet_rupiah) + hilangTitik($request->kernet_zakat_rupiah) + hilangTitik($request->pemasukan_zakat_rupiah);

            $updatePengeluaran = Pemesanan::find($request->id_pemesanan);
            $updatePengeluaran->zakat_persen = $request->zakat_persen;
            $updatePengeluaran->solar = $request->solar;
            $updatePengeluaran->sopir_persen = $request->sopir_persen;
            $updatePengeluaran->sopir_rupiah = $request->sopir_rupiah;
            $updatePengeluaran->sopir_zakat_rupiah = $request->sopir_zakat_rupiah;
            $updatePengeluaran->kernet_persen = $request->kernet_persen;
            $updatePengeluaran->kernet_rupiah = $request->kernet_rupiah;
            $updatePengeluaran->kernet_zakat_rupiah = $request->kernet_zakat_rupiah;
            $updatePengeluaran->pengeluaran_lain = mataUang($totalBiaya);
            $updatePengeluaran->pemasukan_zakat_rupiah = $request->pemasukan_zakat_rupiah;
            $updatePengeluaran->potongan_kas = $request->potongan_kas;
            $updatePengeluaran->pemasukan = $request->pemasukan;
            $updatePengeluaran->total_pengeluaran = mataUang($totalPengeluaran);
            $updatePengeluaran->status_pengeluaran = "1";
            $updatePengeluaran->save();

            Session::forget(['biayaPengeluaranLainLain']);
            Session::flash('pesanSukses', 'Data Pengeluaran Berhasil Disimpan ...');
        } catch (\Exception $e) {
            dd($e);
            die;
            Session::flash('pesanError', 'Data Pengeluaran Gagal Disimpan ...');
        }

        return redirect('pengeluaran/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $var['method'] = 'show';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $listPengeluaran = Pemesanan::find($id);

        return view('pengeluaran.pengeluaran-2', compact('listPengeluaran', 'var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['url'] = $this->url;
        $var['method'] = 'edit';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $listPengeluaran = Pemesanan::find($id);

        return view('pengeluaran.pengeluaran-2', compact('listPengeluaran', 'var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var['url'] = $this->url;
        $totalBiaya = 0;

        try {
            if(!empty(session('biayaPengeluaranLainLain'))){
                $deletePengeluaran = Pengeluaran::where('pemesanan_id', $id)->delete();
                foreach(session('biayaPengeluaranLainLain') as $item){
                    $totalBiaya += $item['biaya_rincian'];
                    $pengeluaran = new Pengeluaran();
                    $pengeluaran->pemesanan_id = $id;
                    $pengeluaran->keperluan = $item['keperluan'];
                    $pengeluaran->biaya = $item['biaya_rincian'];
                    $pengeluaran->input_by = Auth::user()->id;
                    $pengeluaran->save();
                }
            }

            $totalPengeluaran = $totalBiaya + hilangTitik($request->solar) + hilangTitik($request->sopir_rupiah) + hilangTitik($request->sopir_zakat_rupiah);
            $totalPengeluaran +=  hilangTitik($request->kernet_rupiah) + hilangTitik($request->kernet_zakat_rupiah) + hilangTitik($request->pemasukan_zakat_rupiah);

            $updatePengeluaran = Pemesanan::find($id);
            $updatePengeluaran->zakat_persen = $request->zakat_persen;
            $updatePengeluaran->solar = $request->solar;
            $updatePengeluaran->sopir_persen = $request->sopir_persen;
            $updatePengeluaran->sopir_rupiah = $request->sopir_rupiah;
            $updatePengeluaran->sopir_zakat_rupiah = $request->sopir_zakat_rupiah;
            $updatePengeluaran->kernet_persen = $request->kernet_persen;
            $updatePengeluaran->kernet_rupiah = $request->kernet_rupiah;
            $updatePengeluaran->kernet_zakat_rupiah = $request->kernet_zakat_rupiah;
            $updatePengeluaran->pengeluaran_lain = mataUang($totalBiaya);
            $updatePengeluaran->pemasukan_zakat_rupiah = $request->pemasukan_zakat_rupiah;
            $updatePengeluaran->potongan_kas = $request->potongan_kas;
            $updatePengeluaran->pemasukan = $request->pemasukan;
            $updatePengeluaran->total_pengeluaran = mataUang($totalPengeluaran);
            $updatePengeluaran->status_pengeluaran = "1";
            $updatePengeluaran->save();

            Session::forget(['biayaPengeluaranLainLain']);
            Session::flash('pesanSukses', 'Data Pengeluaran Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pengeluaran Gagal Diupdate ...');
        }

        return redirect('pengeluaran'.$var['url']['all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $var['url'] = $this->url;

        try {
            $deletePengeluaran = Pengeluaran::where('pemesanan_id', $id)->delete();

            $update = Pemesanan::find($id);
            $update->total_pengeluaran = NULL;
            $update->status_pengeluaran = NULL;
            $update->save();

            Session::flash('pesanSukses', 'Data Pengeluaran Berhasil Dihapus ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pengeluaran Gagal Dihapus ...');
        }

        return redirect('pengeluaran'.$var['url']['all']);
    }

    public function listPemesanan()
    {        
        $queryPemesanan = Pemesanan::orderBy('id', 'desc')->where('status', 'Lunas')->whereNull('status_pengeluaran');
        (!empty($this->cari))?$queryPemesanan->CariModal($this->cari):'';
        $listPemesanan = $queryPemesanan->paginate($this->jumPerModal);

        return view('pengeluaran.modal-pemesanan', compact('listPemesanan'));
    }

    public function getListPemesanan(Request $request)
    {
        $presentase = Presentase::find(1);
        $listPemesanan = Pemesanan::with('pemesananBus')->find($request->id);

        $listPemesanan['zakat_persen'] = $presentase['zakat'];
        $listPemesanan['sopir_persen'] = $presentase['gaji_sopir'];
        $listPemesanan['kernet_persen'] = $presentase['gaji_kernet'];
        $listPemesanan['potongan_kas'] = $presentase['potongan_kas'];

        $listPemesanan['sopir_zakat_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_sopir'])/100))*(hilangTiTik($presentase['zakat'])/100));
        $listPemesanan['sopir_gaji_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_sopir'])/100))-hilangTiTik($listPemesanan['sopir_zakat_rupiah']));

        $listPemesanan['kernet_zakat_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_kernet'])/100))*(hilangTiTik($presentase['zakat'])/100));
        $listPemesanan['kernet_gaji_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_kernet'])/100))-hilangTiTik($listPemesanan['kernet_zakat_rupiah']));

        $gajiSopirRupiah = mataUang(hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_sopir'])/100));
        $gajiKernetRupiah = mataUang(hilangTiTik($listPemesanan['total_biaya'])*(hilangTiTik($presentase['gaji_kernet'])/100));
        $listPemesanan['pemasukan_zakat_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])-hilangTiTik($gajiSopirRupiah)-hilangTiTik($gajiKernetRupiah))*(hilangTiTik($presentase['zakat'])/100));
        $listPemesanan['pemasukan_rupiah'] = mataUang((hilangTiTik($listPemesanan['total_biaya'])-hilangTiTik($gajiSopirRupiah)-hilangTiTik($gajiKernetRupiah))-hilangTiTik($listPemesanan['pemasukan_zakat_rupiah'])-hilangTiTik($listPemesanan['potongan_kas']));

        return response()->json($listPemesanan);
    }

    public function getListBus2(Request $request)
    {
        Session::forget(['pemesananBus']);
        $var['method'] = Input::get('method', '');
        $var['id'] = Input::get('id', '');
        $var['check-bus'] = 0;

        $listBus = PemesananBus::where('pemesanan_id', $var['id'])->get();
        foreach ($listBus as $item) {
            $bus = Bus::find($item['bus_id']);
            $request->session()->push('pemesananBus', ['id'=> $bus['id'],
                                            'kode' => $bus['kode'], 
                                            'plat_nomor' => $bus['plat_nomor'], 
                                            'warna' => $bus['warna']]);
            $listPemesananBus = session('pemesananBus');
        }
        
        return view('pengeluaran.tabel-pengeluaran-bus', compact('listPemesananBus', 'var'));
    }

    public function getListBiayaRincian(Request $request)
    {
        $var['method'] = "create";

        $request->session()->push('biayaPengeluaranLainLain', [
                                            'keperluan' => $request->keperluan, 
                                            'biaya_rincian' => hilangTiTik($request->biaya)]);
        $listPengeluaranRincian = session('biayaPengeluaranLainLain');
        
        return view('pengeluaran.tabel-pengeluaran-biaya', compact('listPengeluaranRincian', 'var'));
    }

    public function deleteBiayaRincian(Request $request)
    {        
        $var['method'] = "create";
        $listPengeluaranRincian = session('biayaPengeluaranLainLain');
        array_splice($listPengeluaranRincian, $request->id, 1);
        session(['biayaPengeluaranLainLain' =>  $listPengeluaranRincian]);
        $listPengeluaranRincian = session('biayaPengeluaranLainLain');

        return view('pengeluaran.tabel-pengeluaran-biaya', compact('listPengeluaranRincian', 'var'));
    }

    public function getListBiayaRincian2(Request $request)
    {
        Session::forget(['biayaPengeluaranLainLain']);

        $var['method'] = Input::get('method', '');
        $var['id'] = Input::get('id', '');
        $listPengeluaranRincian = array();

        $listPengeluaran = Pengeluaran::where('pemesanan_id', $var['id'])->get();
        foreach ($listPengeluaran as $item) {
            $request->session()->push('biayaPengeluaranLainLain', [
                                            'keperluan' => $item['keperluan'], 
                                            'biaya_rincian' => $item['biaya']]);
            $listPengeluaranRincian = session('biayaPengeluaranLainLain');
        }
        
        return view('pengeluaran.tabel-pengeluaran-biaya', compact('listPengeluaranRincian', 'var'));
    }  

    function cetakPengeluaran($id)
    {
        $var['id'] = $id;
        $pemesanan = Pemesanan::find($id);
        $profil = ProfilPerusahaan::find(1);

        $pdf = PDF::loadView('laporan.pengeluaran', compact('pemesanan', 'profil', 'var'))->setPaper('a4', 'potrait');

        return $pdf->stream('pengeluaran.pdf');
    }
}
