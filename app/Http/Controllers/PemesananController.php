<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Pemesanan;
use App\Models\PemesananBus;
use App\Models\Bus;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Models\ProfilPerusahaan;

class PemesananController extends Controller
{
    private $url;
    private $cari;
    private $jumPerPage = 10;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->cari = Input::get('cari', '');
        $this->url = makeUrl($request->query());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['url'] = $this->url;

        $query = Pemesanan::orderBy('id', 'desc');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listPemesanan = $query->paginate($this->jumPerPage);
        (!empty($this->cari))?$listPemesanan->setPath('pemesanan'.$var['url']['cari']):'';

        return view('pemesanan.pemesanan-1', compact('var', 'listPemesanan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['method'] =  'create';
        Session::forget(['pemesananBus']);

        $noUrut = Pemesanan::max("no_urut");
        $var['no_pemesanan'] = sprintf("%06s",$noUrut+1)."/".Carbon::now()->format('m')."/".Carbon::now()->format('Y');
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();

        return view('pemesanan.pemesanan-2', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $request->all();

            $noUrut = Pemesanan::max("no_urut");
            $input['no_urut'] = $noUrut+1;
            $input['no_pemesanan'] = sprintf("%06s",$noUrut+1)."/".Carbon::now()->format('m')."/".Carbon::now()->format('Y');
            $input['input_by'] = Auth::user()->id;

            if(hilangTiTik($input['uang_muka']) > 0){
                if(hilangTiTik($input['uang_muka'])>=hilangTiTik($input['total_biaya'])){
                    $input['status'] = "Lunas";
                }else{
                    $input['status'] = "Dipesan Sudah DP";
                }
            }else if(hilangTiTik($input['uang_muka']) <= 0){
                $input['status'] = "Dipesan Tanpa DP";
            }

            $simpanPemesanan = Pemesanan::create($input);

            foreach(session('pemesananBus') as $item){
                $pemesananBus = new PemesananBus();
                $pemesananBus->bus_id = $item['id'];
                $pemesananBus->input_by = Auth::user()->id;
                $simpanPemesanan->pemesananBus()->save($pemesananBus);
            }

            Session::forget(['pemesananBus']);
            Session::flash('pesanSukses', 'Data Pemesanan Berhasil Disimpan ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pemesanan Gagal Disimpan ...');
            return redirect('pemesanan/create')->withInput();
        }

        return redirect('pemesanan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $var['method'] = 'show';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $listPemesanan = Pemesanan::find($id);

        return view('pemesanan.pemesanan-2', compact('listPemesanan', 'var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['url'] = $this->url;
        $var['method'] = 'edit';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $listPemesanan = Pemesanan::find($id);

        return view('pemesanan.pemesanan-2', compact('listPemesanan', 'var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var['url'] = $this->url;

        try {
            $updatePemesanan = Pemesanan::find($id);
            $updatePemesanan->update($request->all());

            $deleteBus = PemesananBus::where('pemesanan_id', $id)->delete();
            foreach(session('pemesananBus') as $item){
                $pemesananBus = new PemesananBus();
                $pemesananBus->bus_id = $item['id'];
                $pemesananBus->input_by = Auth::user()->id;
                $updatePemesanan->pemesananBus()->save($pemesananBus);
            }

            Session::forget(['pemesananBus']);
            Session::flash('pesanSukses', 'Data Pemesanan Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pemesanan Gagal Diupdate ...');
        }

        return redirect('pemesanan'.$var['url']['all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $var['url'] = $this->url;

        try {
            $deleteBus = PemesananBus::where('pemesanan_id', $id)->delete();

            $delete = Pemesanan::find($id);
            $delete->delete();

            if($request->nomor == '1'){
                $input = $request->query();
                $input['page'] = '1';
                $var['url'] = makeUrl($input);
            }

            Session::flash('pesanSukses', 'Data Pemesanan Berhasil Dihapus ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pemesanan Gagal Dihapus ...');
        }

        return redirect('pemesanan'.$var['url']['all']);
    }

    public function getListBus(Request $request)
    {
        $var['method'] = "create";
        $tglBerangkat = Carbon::parse($request->tglBerangkat)->format('Y-m-d');
        $tglKepulangan = Carbon::parse($request->tglKepulangan)->format('Y-m-d');
        $listPemesananBus = array(); 

        $var['check-bus'] = PemesananBus::where('bus_id', $request->bus)
            ->whereHas('pemesanan', function ($queryIn) use ($tglBerangkat, $tglKepulangan) {
                $queryIn->whereIn('status', ['Dipesan Sudah DP', 'Lunas'])
                    ->whereIn('id', function ($queryIn2) use ($tglBerangkat, $tglKepulangan) {
                        if($tglBerangkat != $tglKepulangan){
                            $queryIn2->select('id')
                                ->from('pemesanan')
                                ->whereBetween('tanggal_keberangkatan', [$tglBerangkat, $tglKepulangan])
                                ->orWhereBetween('tanggal_kepulangan', [$tglBerangkat, $tglKepulangan]);
                        }else{
                            $queryIn2->select('id')
                                ->from('pemesanan')
                                ->where('tanggal_keberangkatan', '<=', $tglBerangkat)
                                ->where('tanggal_kepulangan', '>=' ,$tglKepulangan);
                        }
                    });
            })->count();

        if($var['check-bus']=='0'){
            if(in_array_r($request->bus, session('pemesananBus'))){
                $listPemesananBus = session('pemesananBus');
            }else{
                $bus = Bus::find($request->bus);
                $request->session()->push('pemesananBus', ['id'=> $request->bus,
                                                'kode' => $bus['kode'], 
                                                'plat_nomor' => $bus['plat_nomor'], 
                                                'warna' => $bus['warna']]);
                $listPemesananBus = session('pemesananBus');
            }
        }else if(session('pemesananBus') != null){
            $listPemesananBus = session('pemesananBus');
        }
        
        return view('pemesanan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    }

    public function deleteBus(Request $request)
    {        
        $var['method'] = "create";
        $var['check-bus'] = 0;
        $listPemesananBus = session('pemesananBus');
        array_splice($listPemesananBus,$request->id,1);
        session(['pemesananBus' =>  $listPemesananBus]);
        $listPemesananBus = session('pemesananBus');

        return view('pemesanan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    }

    public function getListBus2(Request $request)
    {
        Session::forget(['pemesananBus']);
        $var['method'] = Input::get('method', '');
        $var['id'] = Input::get('id', '');
        $var['check-bus'] = 0;

        $listBus = PemesananBus::where('pemesanan_id', $var['id'])->get();
        foreach ($listBus as $item) {
            $bus = Bus::find($item['bus_id']);
            $request->session()->push('pemesananBus', ['id'=> $bus['id'],
                                            'kode' => $bus['kode'], 
                                            'plat_nomor' => $bus['plat_nomor'], 
                                            'warna' => $bus['warna']]);
            $listPemesananBus = session('pemesananBus');
        }
        
        return view('pemesanan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    }   

    function cetakPemesanan($id)
    {
        $var['id'] = $id;
        $pemesanan = Pemesanan::find($id);
        $profil = ProfilPerusahaan::find(1);

        $pdf = PDF::loadView('laporan.bukti-pemesanan', compact('pemesanan', 'profil', 'var'))->setPaper('a4', 'potrait');

        return $pdf->stream('bukti-pemesanan.pdf');
    }
}
