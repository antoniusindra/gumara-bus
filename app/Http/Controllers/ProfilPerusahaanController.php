<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\ProfilPerusahaan;

class ProfilPerusahaanController extends Controller
{

    function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listProfil = ProfilPerusahaan::find(1);

        return view('pengaturan.profil.profil-1', compact('listProfil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        // die;
        try {
            $field = ProfilPerusahaan::find($id);
            $input = $request->all();

            if(!empty($request->gambar)){
                $cekGambar = $this->validate($request, ['gambar'=>'image|mimes:jpeg,jpg,png']);
                $file = $request->file('gambar');
                $namaFile = time().'_'.$file->getClientOriginalName();
                $file->move('upload/logo', $namaFile);

                \File::delete(public_path().'/upload/logo/'.$field->gambar);
                $input['gambar'] = $namaFile;
            }else{
                if(empty($field->gambar)){
                    \File::delete(public_path().'/upload/logo/'.$field->gambar);
                    $namaFile = null;
                    $input['gambar'] = $namaFile;
                }
            }

            $field->update($input);

            Session::flash('pesanSukses', 'Data Profil Perusahaan Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Profil Perusahaan Gagal Diupdate ...');
        }

        return redirect('profil-perusahaan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
