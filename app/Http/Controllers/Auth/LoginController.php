<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Models\ProfilPerusahaan;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/beranda';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $perusahaan = ProfilPerusahaan::find(1);

        return view('login', compact('perusahaan'));
    }

    public function login(Request $request)
    {
        $userRequired = [
            'username' => $request->username,
            'password' => $request->password,
        ];

        $remember_me = $request->has('remember') ? true : false; 

        if(Auth::attempt($userRequired, $remember_me)) {
            //proses
        }

        return redirect('/')
            ->withInput(Input::except('password'))
            ->withErrors(['gagal' => 'Opss ... Login gagal.']);
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        Auth::logout();     
        return redirect('/');
    }
}
