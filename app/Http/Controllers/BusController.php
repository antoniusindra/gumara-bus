<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Bus;

class BusController extends Controller
{
    private $url;
    private $cari;
    private $jumPerPage = 10;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->cari = Input::get('cari', '');
        $this->url = makeUrl($request->query());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['url'] = $this->url;

        $query = Bus::orderBy('id', 'desc');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listBus = $query->paginate($this->jumPerPage);
        (!empty($this->cari))?$listBus->setPath('bus'.$var['url']['cari']):'';

        return view('master-data.bus.bus-1', compact('var', 'listBus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['method'] =  'create';
        return view('master-data.bus.bus-2', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Bus::create($request->all());
            Session::flash('pesanSukses', 'Data Bus Berhasil Disimpan ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Bus Gagal Disimpan ...');
            return redirect('bus/create')->withInput();
        }

        return redirect('bus/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $var['method'] = 'show';
        $listBus = Bus::find($id);;

        return view('master-data.bus.bus-2', compact('listBus', 'var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['url'] = $this->url;
        $var['method'] = 'edit';
        $listBus = Bus::find($id);;

        return view('master-data.bus.bus-2', compact('listBus', 'var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var['url'] = $this->url;

        try {
            $tabel = Bus::find($id);
            $tabel->update($request->all());

            Session::flash('pesanSukses', 'Data Bus Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Bus Gagal Diupdate ...');
        }

        return redirect('bus'.$var['url']['all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $var['url'] = $this->url;

        try {
            $tabel = Bus::find($id);
            $tabel->delete();

            if($request->nomor == '1'){
                $input = $request->query();
                $input['page'] = '1';
                $var['url'] = makeUrl($input);
            }

            Session::flash('pesanSukses', 'Data Bus Berhasil Dihapus ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Bus Gagal Dihapus ...');
        }

        return redirect('bus'.$var['url']['all']);
    }

    public function cekValidasi(Request $request)
    {
        if($request->kolom == 'kode'){
            $kriteria = $request->kode;
        }else if($request->kolom == 'kode_warna'){
            $kriteria = $request->kode_warna;
        }


        if($request->aksi == 'create'){
            $jumlah = Bus::where($request->kolom, $kriteria)->count();

            if ($jumlah != 0) {
                return Response::json(false);
            }else{
                return Response::json(true);
            }
        }else if($request->aksi == 'edit'){
            $jumlah = Bus::where($request->kolom, $kriteria)->count();

            if ($jumlah != 0) {
                $jumlah2 = Bus::where($request->kolom, $kriteria)->where('id', $request->pk)->count();

                if($jumlah2 == 1){
                   return Response::json(true); 
                }else{
                    return Response::json(false);
                }
            }else{
                return Response::json(true);
            }
        }
    }
}
