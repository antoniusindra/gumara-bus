<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Pemesanan;
use App\Models\Bus;
use App\Models\PemesananBus;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class BerandaController extends Controller
{
    private $jumPerPage = 10;

    function __construct(Request $request)
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$var['pesan_belum_dp'] = Pemesanan::where('status', 'Dipesan Tanpa DP')->count();
    	$var['pesan_sudah_dp'] = Pemesanan::where('status', 'Dipesan Sudah DP')->count();
    	$var['lunas'] = Pemesanan::where('status', 'Lunas')->count();
    	$var['pesan_semua'] = $var['pesan_belum_dp'] + $var['pesan_sudah_dp'] + $var['lunas'];

        $listPemesanan = Pemesanan::orderBy('id', 'desc')->paginate($this->jumPerPage);

        return view('beranda', compact('var', 'listPemesanan'));
    }

    public function getKalender()
    {
        $start = Input::get('start', '');
        $end = Input::get('end', '');

        $events = Pemesanan::select('id', 'tujuan as title', 'tanggal_keberangkatan as start', 'tanggal_kepulangan as end')
        	->whereIn('status', ['Dipesan Sudah DP', 'Lunas'])
            ->whereBetWeen('tanggal_keberangkatan', [$start, $end])
            ->whereBetWeen('tanggal_kepulangan', [$start, $end])
            ->get();
        foreach($events as $event)
        {
        	$listBus = PemesananBus::where('pemesanan_id', $event->id)->get();
        	foreach($listBus as $bus){
        		$color = Bus::find($bus['bus_id']);
        		$url = url('/pemesanan/'.$event->id);

        		$listEvent[] = ['title' => 'Tujuan ke '.$event->title.' - '.$color['plat_nomor'],
        						'start' => $event->start,
        						'end' => Carbon::parse($event->end)->addDays(1)->format('Y-m-d'),
        						'url' => $url,
        						'color' =>$color['warna']
        					   ];
        	}
        }

        return json_encode($listEvent);
    }

}
