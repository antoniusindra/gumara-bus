<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use App\Models\Pemesanan;
use App\Models\Pengeluaran;
use App\Models\PemesananBus;
use App\Models\Bus;
use App\Models\Karyawan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;
use App\Models\ProfilPerusahaan;

class PelunasanController extends Controller
{
    private $url;
    private $cari;
    private $jumPerPage = 10;
    private $jumPerModal = 5;

    function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->cari = Input::get('cari', '');
        $this->url = makeUrl($request->query());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $var['url'] = $this->url;

        $query = Pemesanan::orderBy('id', 'desc')->where('status', 'Lunas');
        (!empty($this->cari))?$query->Cari($this->cari):'';
        $listPelunasan = $query->paginate($this->jumPerPage);
        (!empty($this->cari))?$listPelunasan->setPath('pelunasan'.$var['url']['cari']):'';

        return view('pelunasan.pelunasan-1', compact('var', 'listPelunasan'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $var['method'] =  'create';
        Session::forget(['pemesananBus']);

        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $var['user'] = Karyawan::orderBy('id', 'asc')->pluck('nama', 'id')->all();

        return view('pelunasan.pelunasan-2', compact('var'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $updatePemesanan = Pemesanan::find($request->id_pemesanan);
            $updatePemesanan->update($request->all());

            Session::forget(['pemesananBus']);
            Session::flash('pesanSukses', 'Data Pelunasan Berhasil Disimpan ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pelunasan Gagal Disimpan ...');
        }

        return redirect('pelunasan/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $var['method'] = 'show';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $var['user'] = Karyawan::orderBy('id', 'asc')->pluck('nama', 'id')->all();
        $listPelunasan = Pemesanan::find($id);

        return view('pelunasan.pelunasan-2', compact('listPelunasan', 'var'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $var['url'] = $this->url;

        $var['method'] = 'edit';
        $var['bus'] = Bus::orderBy('id', 'asc')->pluck('kode', 'id')->all();
        $var['user'] = Karyawan::orderBy('id', 'asc')->pluck('nama', 'id')->all();
        $listPelunasan = Pemesanan::find($id);

        return view('pelunasan.pelunasan-2', compact('listPelunasan', 'var'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $var['url'] = $this->url;

        try {
            $updatePemesanan = Pemesanan::find($id);
            $updatePemesanan->update($request->all());

            Session::forget(['pemesananBus']);
            Session::flash('pesanSukses', 'Data Pelunasan Berhasil Diupdate ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pelunasan Gagal Diupdate ...');
        }

        return redirect('pelunasan'.$var['url']['all']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $var['url'] = $this->url;

        try {
            $deletePengeluaran = Pengeluaran::where('pemesanan_id', $id)->delete();

            $update = Pemesanan::find($id);
            $update->bayar_kekurangan = NULL;
            $update->pembayaran_kekurangan = NULL;
            $update->bank_kekurangan = NULL;
            $update->user = NULL;
            $update->keterangan = NULL;
            if($update['uang_muka'] > 0 && $update['uang_muka'] == $update['total_biaya']){
                $update->status = "Lunas";
            }else if($update['uang_muka'] > 0 && $update['uang_muka'] < $update['total_biaya']){
                $update->status = "Dipesan Sudah DP";
            }else{
                 $update->status = "Dipesan Tanpa DP";
            }
            $update->total_pengeluaran = NULL;
            $update->status_pengeluaran = NULL;
            $update->save();

            Session::flash('pesanSukses', 'Data Pelunasan Berhasil Dihapus ...');
        } catch (\Exception $e) {
            Session::flash('pesanError', 'Data Pelunasan Gagal Dihapus ...');
        }

        return redirect('pelunasan'.$var['url']['all']);
    }

    public function listPemesanan()
    {        
        $queryPemesanan = Pemesanan::orderBy('id', 'desc')->whereIn('status', ['Dipesan Tanpa DP', 'Dipesan Sudah DP']);
        (!empty($this->cari))?$queryPemesanan->CariModal($this->cari):'';
        $listPemesanan = $queryPemesanan->paginate($this->jumPerModal);

        return view('pelunasan.modal-pemesanan', compact('listPemesanan'));
    }

    public function listPemesanan2()
    {        
        $queryPemesanan = Pemesanan::orderBy('id', 'desc')->whereIn('status', ['Dipesan Tanpa DP', 'Dipesan Sudah DP', 'Lunas']);
        (!empty($this->cari))?$queryPemesanan->CariModal($this->cari):'';
        $listPemesanan = $queryPemesanan->paginate($this->jumPerModal);

        return view('pelunasan.modal-pemesanan', compact('listPemesanan'));
    }

    public function getListPemesanan(Request $request)
    {
        $listPemesanan = Pemesanan::with('pemesananBus')->find($request->id);
        return response()->json($listPemesanan);
    }

    public function getListBus(Request $request)
    {
        $var['method'] = "create";
        $tglBerangkat = Carbon::parse($request->tglBerangkat)->format('Y-m-d');
        $tglKepulangan = Carbon::parse($request->tglKepulangan)->format('Y-m-d');
        $listPemesananBus = array(); 

        $var['check-bus'] = PemesananBus::where('bus_id', $request->bus)
            ->whereHas('pemesanan', function ($queryIn) use ($tglBerangkat, $tglKepulangan) {
                $queryIn->whereIn('status', ['Dipesan Sudah DP', 'Lunas'])
                    ->whereIn('id', function ($queryIn2) use ($tglBerangkat, $tglKepulangan) {
                        $queryIn2->select('id')
                            ->from('pemesanan')
                            ->whereBetween('tanggal_keberangkatan', [$tglBerangkat, $tglKepulangan])
                            ->orWhereBetween('tanggal_kepulangan', [$tglBerangkat, $tglKepulangan]);
                    });
            })->count();

        if($var['check-bus']=='0'){
            if(in_array_r($request->bus, session('pemesananBus'))){
                $listPemesananBus = session('pemesananBus');
            }else{
                $bus = Bus::find($request->bus);
                $request->session()->push('pemesananBus', ['id'=> $request->bus,
                                                'kode' => $bus['kode'], 
                                                'plat_nomor' => $bus['plat_nomor'], 
                                                'warna' => $bus['warna']]);
                $listPemesananBus = session('pemesananBus');
            }
        }else if(session('pemesananBus') != null){
            $listPemesananBus = session('pemesananBus');
        }
        
        return view('pelunasan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    }

    public function deleteBus(Request $request)
    {
        $var['method'] = "create";
        $var['check-bus'] = 0;
        $listPemesananBus = session('pemesananBus');
        array_splice($listPemesananBus,$request->id,1);
        session(['pemesananBus' =>  $listPemesananBus]);
        $listPemesananBus = session('pemesananBus');

        return view('pelunasan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    }

    public function getListBus2(Request $request)
    {
        Session::forget(['pemesananBus']);
        $var['method'] = Input::get('method', '');
        $var['id'] = Input::get('id', '');
        $var['check-bus'] = 0;

        $listBus = PemesananBus::where('pemesanan_id', $var['id'])->get();
        foreach ($listBus as $item) {
            $bus = Bus::find($item['bus_id']);
            $request->session()->push('pemesananBus', ['id'=> $bus['id'],
                                            'kode' => $bus['kode'], 
                                            'plat_nomor' => $bus['plat_nomor'], 
                                            'warna' => $bus['warna']]);
            $listPemesananBus = session('pemesananBus');
        }
        
        return view('pelunasan.tabel-pemesanan-bus', compact('listPemesananBus', 'var'));
    } 

    function cetakSuratJalan($id)
    {
        $var['id'] = $id;
        $pemesanan = Pemesanan::find($id);
        $profil = ProfilPerusahaan::find(1);

        $var['jumlah_hari'] = Carbon::parse($pemesanan['tanggal_kepulangan'])->diffInDays(Carbon::parse($pemesanan['tanggal_keberangkatan']))+1;
        $var['total_bayar'] = mataUang(hilangTitik($pemesanan['uang_muka'])+hilangTitik($pemesanan['bayar_kekurangan']));

        $pdf = PDF::loadView('laporan.surat-jalan', compact('pemesanan', 'profil', 'var'))->setPaper('a4', 'potrait');

        return $pdf->stream('surat-jalan.pdf');
    }

    function cetakSuratJalan2(Request $request)
    {
        $var['id'] = $request->id_pemesanan;
        $user = Karyawan::find($request->user);
        $pemesanan = Pemesanan::find($request->id_pemesanan);
        $profil = ProfilPerusahaan::find(1);

        $var['jumlah_hari'] = Carbon::parse($pemesanan['tanggal_kepulangan'])->diffInDays(Carbon::parse($pemesanan['tanggal_keberangkatan']))+1;
        $var['total_bayar'] = mataUang(hilangTitik($pemesanan['uang_muka'])+hilangTitik($pemesanan['bayar_kekurangan']));

        $pdf = PDF::loadView('laporan.surat-jalan', compact('pemesanan', 'profil', 'var', 'user'))->setPaper('a4', 'potrait');

        return $pdf->stream('surat-jalan.pdf');
    }
}
