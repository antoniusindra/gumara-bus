<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'telepon', 'username', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password) {
        $this->attributes['password'] = bcrypt($password);
    }

    public function scopeCari($query, $cari) {
        return $query->where('name', 'like', '%'.$cari.'%')
                ->orWhere('email', 'like', '%'.$cari.'%')
                ->orWhere('telepon', 'like', '%'.$cari.'%')
                ->orWhere('username', 'like', '%'.$cari.'%');
    }
}
