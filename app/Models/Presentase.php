<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Presentase extends Model
{
    protected $table = 'presentase';
    protected $fillable = [
        'zakat', 'gaji_sopir', 'gaji_kernet', 'potongan_kas'
    ];

    public function setZakatAttribute($data) {
        $this->attributes['zakat'] = hilangTiTik($data);
    }

    public function getZakatAttribute($value){
       return mataUang($value);
    }

    public function setGajiSopirAttribute($data) {
        $this->attributes['gaji_sopir'] = hilangTiTik($data);
    }

    public function getGajiSopirAttribute($value){
        return mataUang($value);
    }

    public function setGajiKernetAttribute($data) {
        $this->attributes['gaji_kernet'] = hilangTiTik($data);
    }

    public function getGajiKernetAttribute($value){
        return mataUang($value);
    }

    public function setPotonganKasAttribute($data) {
        $this->attributes['potongan_kas'] = hilangTiTik($data);
    }

    public function getPotonganKasAttribute($value){
        return mataUang($value);
    }
}
