<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\PemesananBus;
use App\Models\Pemesanan;
use Carbon\Carbon;

class Bus extends Model
{
    protected $table = 'bus';
    protected $fillable = [
        'kode', 'merk', 'plat_nomor', 'warna'
    ];

    public function scopeCari($query, $cari) {
        return $query->where('kode', 'like', '%'.$cari.'%')
                ->orWhere('merk', 'like', '%'.$cari.'%')
                ->orWhere('plat_nomor', 'like', '%'.$cari.'%');
    }

    public function keteranganJadwal($tanggal, $bulan, $tahun, $busId) {
    	$tanggal = Carbon::create($tahun, $bulan, $tanggal, 0, 0, 0)->format('Y-m-d');

    	$pemesananDetail = PemesananBus::where('bus_id', $busId)
    			->whereHas('pemesanan', function ($queryHas) use ($tanggal) {
                $queryHas->where('tanggal_keberangkatan', '<=', $tanggal)
                	->where('tanggal_kepulangan', '>=', $tanggal)
                	->whereIn('status', ['Dipesan Sudah DP', 'Lunas']);
            })->first();

    	$pemesanan = Pemesanan::find($pemesananDetail['pemesanan_id']);
    	if($pemesanan['nama'] != "" && $pemesanan['tujuan'] != ""){
    		$keterangan = $pemesanan['nama']." tujuan ke ".$pemesanan['tujuan'];
    	}else{
    		$keterangan = "";
    	}
    	
    	
		return $keterangan;
    }
}
