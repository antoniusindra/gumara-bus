<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProfilPerusahaan extends Model
{
    protected $table = 'profil';
    protected $fillable = [
        'nama', 'alamat', 'kota', 'email', 'telepon', 'gambar'
    ];
}
