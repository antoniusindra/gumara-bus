<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{
    protected $table = 'karyawan';
    protected $fillable = [
        'nik', 'nama', 'alamat', 'email', 'no_hp'
    ];

    public function scopeCari($query, $cari) {
        return $query->where('nik', 'like', '%'.$cari.'%')
                ->orWhere('nama', 'like', '%'.$cari.'%')
                ->orWhere('email', 'like', '%'.$cari.'%') 
                ->orWhere('no_hp', 'like', '%'.$cari.'%');
    }
}
