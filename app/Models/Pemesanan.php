<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Pemesanan extends Model
{
    protected $table = 'pemesanan';
    protected $fillable = [
        'no_urut', 'no_pemesanan', 'nama', 'telepon', 'pj_rombongan', 'telp_pj_rombongan', 'tanggal_keberangkatan', 'jam_keberangkatan',
        'tanggal_kepulangan', 'jam_kepulangan', 'tujuan', 'alamat_penjemputan', 'jumlah_armada', 'harga_satuan',
        'biaya_sewa', 'biaya_tambahan', 'total_biaya', 'uang_muka', 'sisa_pembayaran', 'pembayaran', 'bank', 'catatan', 'status', 
        'input_by', 'bayar_kekurangan', 'pembayaran_kekurangan', 'bank_kekurangan', 'user', 'keterangan', 'total_pengeluaran',
        'status_pengeluaran', 'zakat_persen', 'solar', 'sopir_persen', 'sopir_rupiah', 'sopir_zakat_rupiah', 'kernet_persen',
        'kernet_rupiah', 'kernet_zakat_rupiah', 'pengeluaran_lain', 'pemasukan_zakat_rupiah', 'potongan_kas', 'pemasukan'
    ];

    public function scopeCari($query, $cari) {
        return $query->where('no_pemesanan', 'like', '%'.$cari.'%')
                ->orWhere('nama', 'like', '%'.$cari.'%')
                ->orWhere('telepon', 'like', '%'.$cari.'%') 
                ->orWhere('tanggal_keberangkatan', 'like', '%'.$cari.'%') 
                ->orWhere('tanggal_kepulangan', 'like', '%'.$cari.'%') 
                ->orWhere('tujuan', 'like', '%'.$cari.'%') 
                ->orWhere('status', 'like', '%'.$cari.'%');
    }

    public function scopeCariModal($query, $cari) {
        return $query->where('no_pemesanan', 'like', '%'.$cari.'%')
                ->orWhere('nama', 'like', '%'.$cari.'%')
                ->orWhere('telepon', 'like', '%'.$cari.'%') 
                ->orWhere('tanggal_keberangkatan', 'like', '%'.$cari.'%') 
                ->orWhere('tanggal_kepulangan', 'like', '%'.$cari.'%') 
                ->orWhere('tujuan', 'like', '%'.$cari.'%');
    }

    public function setTanggalKeberangkatanAttribute($tanggal) {
        $this->attributes['tanggal_keberangkatan'] = Carbon::parse($tanggal)->format('Y-m-d');
    }

    public function getTanggalKeberangkatanAttribute($value){
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getJamKeberangkatanAttribute($value){
        return Carbon::parse($value)->format('H:i');
    }

    public function setTanggalKepulanganAttribute($tanggal) {
        $this->attributes['tanggal_kepulangan'] = Carbon::parse($tanggal)->format('Y-m-d');
    }

    public function getTanggalKepulanganAttribute($value){
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function getJamKepulanganAttribute($value){
        return Carbon::parse($value)->format('H:i');
    }

    public function setHargaSatuanAttribute($data) {
         $this->attributes['harga_satuan'] = hilangTiTik($data);
    }

    public function getHargaSatuanAttribute($value){
        return mataUang($value);
    }

    public function setBiayaSewaAttribute($data) {
         $this->attributes['biaya_sewa'] = hilangTiTik($data);
    }

    public function getBiayaSewaAttribute($value){
        return mataUang($value);
    }

    public function setBiayaTambahanAttribute($data) {
         $this->attributes['biaya_tambahan'] = hilangTiTik($data);
    }

    public function getBiayaTambahanAttribute($value){
        return mataUang($value);
    }

    public function setTotalBiayaAttribute($data) {
         $this->attributes['total_biaya'] = hilangTiTik($data);
    }

    public function getTotalBiayaAttribute($value){
        return mataUang($value);
    }

    public function setUangMukaAttribute($data) {
         $this->attributes['uang_muka'] = hilangTiTik($data);
    }

    public function getUangMukaAttribute($value){
        return mataUang($value);
    }

    public function setTotalPengeluaranAttribute($data) {
        $this->attributes['total_pengeluaran'] = hilangTiTik($data);
    }

    public function getTotalPengeluaranAttribute($value){
        return mataUang($value);
    }

    public function setSisaPembayaranAttribute($data) {
         $this->attributes['sisa_pembayaran'] = hilangTiTik($data);
    }

    public function getSisaPembayaranAttribute($value){
        return mataUang($value);
    }

    public function setBayarKekuranganAttribute($data) {
         $this->attributes['bayar_kekurangan'] = hilangTiTik($data);
    }

    public function getBayarKekuranganAttribute($value){
        return mataUang($value);
    }

    public function setZakatPersenAttribute($data) {
        $this->attributes['zakat_persen'] = hilangTiTik($data);
    }

    public function getZakatPersenAttribute($value){
       return mataUang($value);
    }

    public function setSolarAttribute($data) {
        $this->attributes['solar'] = hilangTiTik($data);
    }

    public function getSolarAttribute($value){
       return mataUang($value);
    }

    public function setSopirPersenAttribute($data) {
        $this->attributes['sopir_persen'] = hilangTiTik($data);
    }

    public function getSopirPersenAttribute($value){
       return mataUang($value);
    }

    public function setSopirRupiahAttribute($data) {
        $this->attributes['sopir_rupiah'] = hilangTiTik($data);
    }

    public function getSopirRupiahAttribute($value){
       return mataUang($value);
    }

    public function setSopirZakatRupiahAttribute($data) {
        $this->attributes['sopir_zakat_rupiah'] = hilangTiTik($data);
    }

    public function getSopirZakatRupiahAttribute($value){
       return mataUang($value);
    }

    public function setKernetPersenAttribute($data) {
        $this->attributes['kernet_persen'] = hilangTiTik($data);
    }

    public function getKernetPersenAttribute($value){
       return mataUang($value);
    }

    public function setKernetRupiahAttribute($data) {
        $this->attributes['kernet_rupiah'] = hilangTiTik($data);
    }

    public function getKernetRupiahAttribute($value){
       return mataUang($value);
    }

    public function setKernetZakatRupiahAttribute($data) {
        $this->attributes['kernet_zakat_rupiah'] = hilangTiTik($data);
    }

    public function getKernetZakatRupiahAttribute($value){
       return mataUang($value);
    }

    public function setPengeluaranLainAttribute($data) {
        $this->attributes['pengeluaran_lain'] = hilangTiTik($data);
    }

    public function getPengeluaranLainAttribute($value){
       return mataUang($value);
    }

    public function setPemasukanZakatRupiahAttribute($data) {
        $this->attributes['pemasukan_zakat_rupiah'] = hilangTiTik($data);
    }

    public function getPemasukanZakatRupiahAttribute($value){
       return mataUang($value);
    }

    public function setPemasukanAttribute($data) {
        $this->attributes['pemasukan'] = hilangTiTik($data);
    }

    public function getPemasukanAttribute($value){
       return mataUang($value);
    }

    public function setPotonganKasAttribute($data) {
        $this->attributes['potongan_kas'] = hilangTiTik($data);
    }

    public function getPotonganKasAttribute($value){
       return mataUang($value);
    }

    public function pemesananBus() {
        return $this->hasMany('App\Models\PemesananBus', 'pemesanan_id');
    }

    public function karyawan() {
        return $this->belongsTo('App\Models\Karyawan', 'user');
    }
}
