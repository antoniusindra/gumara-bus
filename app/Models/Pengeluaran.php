<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    protected $table = 'pengeluaran';
    protected $fillable = [
        'pemesanan_id', 'keperluan', 'biaya', 'input_by'
    ];

    public function pemesanan() {
        return $this->belongsTo('App\Models\Pemesanan', 'pemesanan_id');
    }
}
