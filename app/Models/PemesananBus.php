<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PemesananBus extends Model
{
    protected $table = 'pemesanan_bus';
    protected $fillable = [
        'pemesanan_id', 'bus_id', 'input_by'
    ];

    public function pemesanan() {
        return $this->belongsTo('App\Models\Pemesanan', 'pemesanan_id');
    }

    public function bus() {
        return $this->belongsTo('App\Models\Bus', 'bus_id');
    }
}
