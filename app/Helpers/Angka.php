<?php
	function mataUang($data){
		$nilai = number_format($data,2,',','.');
		return $nilai;
	}

	function hilangTiTik($data){
		$nilai2 = $data;

		if(strpos($data,".") !== false){
			if(strpos($data,",") !== false){
				$nilai = str_replace(".", "", $data);
				$nilai2 = str_replace(",", ".", $nilai);
			}
		}else if(strpos($data,",") !== false){
			$nilai2 = str_replace(",", ".", $data);
		}

		if($nilai2 == ""){
			$nilai2 = 0;
		}
		
		return $nilai2;
	}

	function in_array_r($item , $array){
	    return preg_match('/"'.$item.'"/i' , json_encode($array));
	}

	function tanggal_format_indonesia($tgl){
        $tanggal = substr($tgl,8,2);
         $bulan   = getBulan(substr($tgl,5,2));
        $tahun   = substr($tgl,0,4);
        return $tanggal.' '.$bulan.' '.$tahun;
 	}
 
    function getBulan($bln){
        switch ($bln){
          case 1: 
          return "Januari";
          break;
          case 2:
          return "Februari";
          break;
          case 3:
          return "Maret";
          break;
          case 4:
          return "April";
          break;
          case 5:
          return "Mei";
          break;
          case 6:
          return "Juni";
          break;
          case 7:
          return "Juli";
          break;
          case 8:
          return "Agustus";
          break;
          case 9:
          return "September";
          break;
          case 10:
          return "Oktober";
          break;
          case 11:
          return "November";
          break;
          case 12:
          return "Desember";
          break;
        }
    }

    function nama_hari($hari){
        if($hari == "Mon"){
            $nama = "Senin";
        }else if($hari == "Tue"){
            $nama = "Selasa";
        }else if($hari == "Wed"){
            $nama = "Rabu";
        }else if($hari == "Thu"){
            $nama = "Kamis";
        }else if($hari == "Fri"){
            $nama = "Jumat";
        }else if($hari == "Sat"){
            $nama = "Sabtu";
        }else if($hari == "Sun"){
            $nama = "Minggu";
        }

        return $nama;
    } 
?>