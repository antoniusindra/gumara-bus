<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon2.png') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('elite/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css') }}" rel="stylesheet">
    <!-- animation CSS -->
    <link href="{{ asset('elite/css/animate.css') }}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('elite/css/style.css') }}" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{ asset('elite/css/colors/blue.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="http://www.w3schools.com/lib/w3data.js"></script>
    <style type="text/css">
        /* Form Error */
        label.error {
            color: #c4002e;
            font-weight: 300;
        }
    </style>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <section id="wrapper" class="login-register">
        <div class="login-box" style="opacity: 0.80;">
            <div class="white-box">
                @if($errors->has('gagal'))
                    <div class="alert alert-danger alert-dismissable" style="margin-bottom: 20px">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <span class="semi-bold">{{ $errors->first('gagal') }}</span>
                    </div>
                @endif

                {!! Form::open(['method'=>'POST', 'route'=>'login-post', 'class'=>'form-horizontal form-material', 'id'=>'loginform']) !!}
                    <center><h3 class="box-title m-b-20">Sistem Pemesanan Bus {{ $perusahaan['nama'] }}</h3></center>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Username']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Inputkan Password']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a>
                            </div>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </section>
    <!-- jQuery -->
    <script src="{{ asset('elite/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('elite/bootstrap/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('elite/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('elite/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('elite/js/waves.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('elite/js/custom.min.js') }}"></script>
    <!--Style Switcher -->
    <script src="{{ asset('elite/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <!--Validate-->
    <script src="{{ asset('pluginku/jquery-validation-1.15.1/dist/jquery.validate.js') }}"></script>
    <script src="{{ asset('pluginku/jquery-validation-1.15.1/dist/additional-methods.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {  
            $("#loginform").validate({
                rules: {
                    username: "required",
                    password: "required"
                },
                messages: {
                    username: "Kolom username harus diisi",
                    password: "Kolom password harus diisi",
                }
            });
        });
    </script>
</body>

</html>
