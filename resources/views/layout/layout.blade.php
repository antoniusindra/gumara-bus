<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon2.png') }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('elite/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css') }}" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{ asset('elite/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('elite/plugins/bower_components/dropify/dist/css/dropify.min.css') }}">
    <!-- toast CSS -->
    <link href="{{ asset('elite/plugins/bower_components/toast-master/css/jquery.toast.css') }}" rel="stylesheet">
     <!--alerts CSS -->
    <link href="{{ asset('elite/plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <!-- animation CSS -->
    <link href="{{ asset('elite/css/animate.css') }}" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="{{ asset('elite/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') }}" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="{{ asset('elite/plugins/bower_components/calendar/dist/fullcalendar.css') }}" rel="stylesheet" />

    <link href="{{ asset('elite/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('elite/plugins/bower_components/custom-select/custom-select.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('elite/plugins/bower_components/switchery/dist/switchery.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}" rel="stylesheet" />
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('elite/plugins/bower_components/multiselect/css/multi-select.css') }}" rel="stylesheet" type="text/css" />

    <!-- Color picker plugins css -->
    <link href="{{ asset('elite/plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css') }}" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="{{ asset('elite/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="{{ asset('elite/css/style.css') }}" rel="stylesheet">
    <!-- color CSS you can use different color css from css/colors folder -->
    <link href="{{ asset('elite/css/colors/default.css') }}" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
    <script src="http://www.w3schools.com/lib/w3data.js"></script>

    <style type="text/css">
        /* Form Error */
        label.error {
            color: #c4002e;
            font-weight: 300;
        }
        .form-control.error {
            border: 1px solid #c4002e;
        }

        /*Sweet Alert*/
        .sa-button-container {
            display: -webkit-flex;
            display: flex;
            -webkit-justify-content: center;
                      justify-content: center;
        }
        .sa-button-container .cancel {
            -webkit-order: 2;
                    order: 2;
        }
        .sa-button-container .sa-confirm-button-container {
            -webkit-order: 1;
                    order: 1;
        }

        /*Pagination*/
        .pagination .active span {
            background-color: #03a9f3;
            border-color: #03a9f3;
        }
        .pagination .active span:hover {
            background-color: #03a9f3;
            border-color: #03a9f3;
        }

        td .fc-event-container {
            padding-right:10px; 
            padding-left:10px;
        }
    </style>
</head>

<body class="fix-header">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href=""><b><img src="{{ asset('elite/plugins/images/eliteadmin-logo.png') }}" alt="home" /></b><span class="hidden-xs"><img src="{{ asset('elite/plugins/images/eliteadmin-text copy2.png') }}" alt="home" /></span></a></div>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{ asset('upload/avatar.jpg') }}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->name }}</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li><a href="{{ url('info-pengguna') }}"><i class="ti-user"></i> Info Pengguna</a></li>
                            <li><a href="{{ url('ganti-password') }}"><i class="ti-unlock"></i> Ganti Password</a></li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <a href="" onclick="event.preventDefault(); document.getElementById('logout-form-1').submit();">
                                    <i class="fa fa-power-off"></i> Logout
                                </a>

                                <form id="logout-form-1" action="{{ route('logout') }}" method="post" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li> <a href="{{ url('beranda') }}" class="waves-effect @if(isset($varGlobal['beranda'])) {{ $varGlobal['beranda'] }} @endif"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Beranda</a></li>
                    
                    <li> <a href="#" class="waves-effect @if(isset($varGlobal['master-data'])) {{ $varGlobal['master-data'] }} @endif"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Master Data</a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('bus') }}">Bus</a> </li>
                            <li> <a href="{{ url('karyawan') }}">Karyawan</a> </li>
                        </ul>
                    </li>
                    <li><a href="{{ url('pemesanan') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Pemesanan</a></li>
                    <li><a href="{{ url('pelunasan') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Pelunasan</a></li>
                    <li><a href="{{ url('pengeluaran') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Pengeluaran</a></li>
                    <li><a href="{{ url('laporan') }}" class="waves-effect"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Laporan</a></li>
                    
                    <li> <a href="#" class="waves-effect @if(isset($varGlobal['pengaturan'])) {{ $varGlobal['pengaturan'] }} @endif"><i data-icon="v" class="linea-icon linea-basic fa-fw"></i>Pengaturan</a>
                        <ul class="nav nav-second-level">
                            {{-- <li> <a href="{{ url('profil-perusahaan') }}">Profil Perusahaan</a> </li> --}}
                            <li> <a href="{{ url('user') }}">User</a> </li>
                            <li> <a href="{{ url('presentase') }}">Presentase</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">

            @if(Session::has('pesanSukses'))
                <script type="text/javascript">
                    function codeAddress() {
                        swal({
                            title: "Berhasil !",   
                            text: "<b>{{ Session::get('pesanSukses') }}</b>",   
                            type: "success", 
                            html: true
                        });
                    }
                    window.onload = codeAddress;
                </script>
            @endif

            @if(Session::has('pesanError'))
                <script type="text/javascript">
                    function codeAddress() {
                        swal({
                            title: "Gagal !",   
                            text: "<b>{{ Session::get('pesanError') }}</b>",   
                            type: "error", 
                            html: true
                        });
                    }
                    window.onload = codeAddress;
                </script>
            @endif

            <div class="container-fluid">

                @yield('konten')
                
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center"> {{ \Carbon\Carbon::now()->format('Y' )}} &copy; Sistem Informasi Pemesanan Bus PT Gumara Trans Jaya. Development By <a href="https://www.facebook.com/antoniusindralegowo" style="color: blue;">Captain Nemo</a> </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{ asset('elite/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('elite/bootstrap/dist/js/tether.min.js') }}"></script>
    <script src="{{ asset('elite/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--Counter js -->
    <script src="{{ asset('elite/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('elite/js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src="{{ asset('elite/js/waves.js') }}"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js') }}"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') }}"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="{{ asset('elite/plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('elite/js/custom.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/custom-select/custom-select.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('elite/plugins/bower_components/bootstrap-select/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <!--Style Switcher -->
    <script src="{{ asset('elite/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <!--Validate-->
    <script src="{{ asset('pluginku/jquery-validation-1.15.1/dist/jquery.validate.js') }}"></script>
    <!-- jQuery file upload -->
    <script src="{{ asset('elite/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <!--mask money-->
    <script src="{{ asset('pluginku/maskmoney/dist/jquery.maskMoney.js') }}"></script>
    <script src="{{ asset('pluginku/chart-js/Chart.js') }}"></script>

    <script type="text/javascript">

        function mataUang(form){ 
            var nilai = $("#"+form).val();
            var isValid = /^[0-9,.-]*$/.test(nilai);

            if(isValid){
                $("#"+form).maskMoney({ thousands: '.', decimal: ',', allowZero: true, allowNegative: true });    
            }else{
                var nilai = nilai.substring(0, nilai.length - 1);
                $("#"+form).val(nilai)
            }
        }

        function mataUang2(nilai){
            if($.isNumeric(nilai)) {
                var value = nilai.toFixed(2);
                var pecah = value.split('.');
                var pecah1 = formatangka_titik(pecah[0]);
                var value1 = pecah1+","+pecah[1];

                return value1;
            }else{
                return "0,00";
            }
        }

        function formatangka_titik(nilai){
            a = nilai;
            b = a.replace(/[^\d]/g,"");
            c = "";
            panjang = b.length;
            j = 0;
            for (i = panjang; i > 0; i--){
                j = j + 1;
                if (((j % 3) == 1) && (j != 1)){
                    c = b.substr(i-1,1) + "." + c;
                } else {
                    c = b.substr(i-1,1) + c;
                }
            }
            if(nilai < 0){
                return "-"+c;
            }else if(nilai >= 0){
                return c;
            }
        }

        function formatMataUang(nilai){
            var bilangan = findAndReplace(nilai, '.', '');
                bilangan = findAndReplace(bilangan, ',', '.');

            return bilangan;
        }

        function findAndReplace(string, target, replacement) {
            var i = 0, length = string.length;
            for (i; i < length; i++) {
                string = string.replace(target, replacement);
            }
            return string;
        }

        $(".counter").counterUp({
            delay: 100,
            time: 1200
        });

        $().ready(function() {   
            //Confirm Message
            $('button.btn-danger').on('click', function(e){
                e.preventDefault();
                var $self = $(this);
                swal({   
                    reverseButton: true,
                    title: "Data yakin dihapus ?",   
                    text: "<b>Mohon diteliti sebelum menghapus data</b>",   
                    type: "warning",  
                    confirmButtonText: "Hapus",   
                    cancelButtonText: "Batal",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",    
                    closeOnConfirm: false,
                    html: true
                }, function(){ 
                    $self.parents(".delete_form").submit();
                });
            });

            $('.clockpicker').clockpicker({
                autoclose: true,
                'default': 'now'
            });

            $('.datepicker-autoclose').datepicker({
                autoclose: true,
                format: "dd-mm-yyyy",
                todayHighlight: true
            });

            $(".select2").select2();

            jQuery('#date-range').datepicker({
                toggleActive: true,
                autoclose: true,
                format: "dd-mm-yyyy",
                orientation: "bottom"
            });
        });

        $.ajaxSetup({
            headers:{
                'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    @yield('javascript')
</body>

</html>
