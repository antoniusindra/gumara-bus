@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Beranda</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li class="active">Beranda</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <!-- /.row -->
    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <div class="row row-in">
                    <div class="col-lg-3 col-sm-6 row-in-br">
                        <div class="col-in row">
                            <div class="col-md-7 col-sm-7 col-xs-7"> <i data-icon="E" class="linea-icon linea-basic"></i>
                                <h5 class="text-muted vb">Jumlah Semua Pemesanan</h5>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <h3 class="counter text-right m-t-15 text-danger">{{ $var['pesan_semua'] }}</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 row-in-br  b-r-none">
                        <div class="col-in row">
                            <div class="col-md-7 col-sm-7 col-xs-7"> <i class="linea-icon linea-basic" data-icon="&#xe01b;"></i>
                                <h5 class="text-muted vb">Jumlah Pemesanan Tanpa DP</h5>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <h3 class="counter text-right m-t-15 text-megna">{{ $var['pesan_belum_dp'] }}</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-megna" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6 row-in-br">
                        <div class="col-in row">
                            <div class="col-md-7 col-sm-7 col-xs-7"> <i class="linea-icon linea-basic" data-icon="&#xe00b;"></i>
                                <h5 class="text-muted vb">Jumlah Pemesanan Sudah DP</h5>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <h3 class="counter text-right m-t-15 text-primary">{{ $var['pesan_sudah_dp'] }}</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-sm-6  b-0">
                        <div class="col-in row">
                            <div class="col-md-7 col-sm-7 col-xs-7"> <i class="linea-icon linea-basic" data-icon="&#xe016;"></i>
                                <h5 class="text-muted vb">Jumlah Pemesanan Lunas</h5>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <h3 class="counter text-right m-t-15 text-success">{{ $var['lunas'] }}</h3>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row -->

    <div class="row">
        <div class="col-md-12 col-lg-12 col-sm-12">
             <div class="white-box">
                <div id="kalenderku"></div>
            </div>
        </div>

        <div class="col-md-12 col-lg-12 col-sm-12">
            <div class="white-box">
                <h3 class="box-title">PEMESANAN TERBARU</h3>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="text-align: center;"><b>No. Pemesanan</b></th>
                                <th style="text-align: center;"><b>Nama</b></th>
                                <th style="text-align: center;"><b>Telepon</b></th>
                                <th style="text-align: center;"><b>Tgl Berangkat</b></th>
                                <th style="text-align: center;"><b>Tgl Kepulangan</b></th>
                                <th style="text-align: center;"><b>Tujuan</b></th>
                                <th style="text-align: center;"><b>Bus</b></th>
                                <th style="text-align: center;"><b>Status</b></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                \Carbon\Carbon::setLocale('id');
                            @endphp
                            @foreach($listPemesanan as $item)
                                @if($item['status']=='Dipesan Tanpa DP')
                                    @php $warna = "label-danger"; @endphp
                                @elseif($item['status']=='Dipesan Sudah DP')
                                    @php $warna = "label-success"; @endphp
                                @elseif($item['status']=='Lunas')
                                    @php $warna = "label-info"; @endphp
                                @endif
                            <tr>
                                <td>{{ $item['no_pemesanan'] }}</td>
                                <td>{{ $item['nama'] }}</td>
                                <td>{{ $item['telepon'] }}</td>
                                <td>{{ $item['tanggal_keberangkatan'] }}</td>
                                <td>{{ $item['tanggal_kepulangan'] }}</td>
                                <td>{{ $item['tujuan'] }}</td>
                                <td>
                                    @foreach($item->pemesananBus as $item2)
                                        {{ $item2->bus['plat_nomor'] }} </br>
                                    @endforeach
                                </td>
                                <td><span class="label label-rounded {{ $warna }}">{{ $item['status'] }}</span></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="row">
                <div class="col-md-6 offset-md-6">
                     {{ $listPemesanan->render() }}
                </div>
            </div>
            </div>
        </div>

    </div>
    <!-- /.row -->
@endsection

@section('javascript')    
    <!-- Calendar JavaScript -->
    <script src="{{ asset('elite/plugins/bower_components/calendar/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('elite/plugins/bower_components/moment/moment.js') }}"></script>
    <script src="{{ asset('pluginku/fullcalendar-3.6.1/fullcalendar.js') }}"></script>
    <script src="{{ asset('pluginku/fullcalendar-3.6.1/locale/id.js') }}"></script>

    <script type="text/javascript">
        //kalender
        $('#kalenderku').fullCalendar({
            timeFormat: 'H:mm',
            height: 800,
            weekends: true,
            header: {
                left: 'prev,next today',
                center: 'title',
            },
            editable: false,
            eventLimit: true, // allow "more" link when too many events
            events: {
                url: '{{ url('/beranda/kalender-1') }}',
                success: function (data) {
                    console.log(data);
                },
                error: function(data) {
                    // console.log(data);
                    // alert("cannot load json");
                }
            }
        });
    </script>
@endsection

