<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon.png') }}">
        <link href="{{ asset('elite/css/print.css') }}" rel="stylesheet">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>
    </head>
    <body>
       <center><h3>JADWAL BUS GUMARA TRANSPORT</h3></center>
       <p><b>BULAN : {{ strtoupper(getBulan($var['bulan'])) }} {{ $var['tahun'] }}</b></p>
       @php
            $jumTanggal = \Carbon\Carbon::create($var['tahun'], $var['bulan'], 1, 0, 0, 0)->endOfMonth()->format('d');
       @endphp

       <table class="table-border" cellspacing="1" cellpadding="1">
            <tr>
                <td style="text-align: center; background-color: #c2c1c3; width::35px;"><b>TGL</b></td>
                @foreach($bus as $itemBus)
                    <td style="text-align: center; background-color: #c2c1c3;"><b>{{ $itemBus['plat_nomor'] }}</b></td>
                @endforeach
            </tr>

            @for($i=1; $i<=$jumTanggal; $i++)
                <tr>
                    <td style="text-align: center;">{{ $i }}</td>
                    @foreach($bus as $itemBus2)
                        @php
                            $keterangan = $itemBus2->keteranganJadwal($i, $var['bulan'], $var['tahun'], $itemBus2['id']);
                        @endphp
                        <td>{{ $keterangan }}</td>
                    @endforeach
                </tr>
            @endfor
       </table>
    </body>
</html>
