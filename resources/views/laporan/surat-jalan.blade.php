<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon.png') }}">
    <link href="{{ asset('elite/css/print.css') }}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style type="text/css">
		.page-border { border:5px black; border-style: double; padding: 10px;}}
        table tbody tr td {font-family:Arial;font-size:18px}
    </style>
    <title>{{ config('app.name') }}</title>
</head>
	@foreach($pemesanan->pemesananBus as $busItem)
    	<body class="page-border">
		 	<h3 style="color: red;">GUMARA TRANSPORT</h3>
		 	<h4>Jalan Semarang - Boja Km 2 Tampingan</h4>
		 	<h4>Telp (0294) 571 386 / 0852 9046 4191</h4>
		 	<br>
		 	<center><h3><u>SURAT KETERANGAN JALAN</u></h3></center>
            <br>
            <table class="table-no-border" cellspacing="1" cellpadding="5">
                <tbody>
                    <tr>
                        <td width="35%"><b>JENIS / NO POL</b></td>
                        <td>: {{ $busItem->bus['merk'] }} / {{ $busItem->bus['plat_nomor'] }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>HARI / TANGGAL</b></td>
                        <td>: {{ nama_hari(date('D', strtotime($pemesanan['tanggal_keberangkatan']))) }} / {{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_keberangkatan']))) }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>JAM</b></td>
                        <td>: {{ $pemesanan['jam_keberangkatan'] }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>USER / OLEH</b></td>
                        <td>: {{ $user['nama'] }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>NO TELP / HP USER</b></td>
                        <td>: {{ $user['no_hp'] }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>KETERANGAN</b></td>
                        <td>: </td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table>
                <tr>
                    <td><center><b>Tanda Tangan dan Nama Terang</b></center></td>
                    <td width="60%">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><center>.........................................</center></td>
                    <td style="text-align: right; padding-right: 40px;"><b> *) Dikirim / Diambil</b></td>
                </tr>
            </table>
            <br>
            <center><h3><u>KEMBALI</u></h3></center>
            <table class="table-no-border" cellspacing="1" cellpadding="5">
                <tbody>
                    <tr>
                        <td width="35%"><b>HARI / TANGGAL</b></td>
                        <td>: {{ nama_hari(date('D', strtotime($pemesanan['tanggal_kepulangan']))) }} / {{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_kepulangan']))) }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>JAM</b></td>
                        <td>: {{ $pemesanan['jam_kepulangan'] }}</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>JUMLAH HARI</b></td>
                        <td>: {{ $var['jumlah_hari'] }} Hari</td>
                    </tr>
                    <tr>
                        <td width="35%"><b>TOTAL BAYAR</b></td>
                        <td>: Rp {{ $var['total_bayar'] }}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <table>
                <tr>
                    <td><center><b>Diterima Oleh</b></center></td>
                    <td width="60%">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><center>.........................................</center></td>
                    <td style="text-align: right; padding-right: 40px;"><b> *) Diantar / Diambil</b></td>
                </tr>
            </table>
            <br>
            <b>*) Coret yang tidak perlu</b>
    	</body>
	@endforeach
</html>
