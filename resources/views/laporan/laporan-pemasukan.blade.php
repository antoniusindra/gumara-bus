<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon.png') }}">
    <link href="{{ asset('elite/css/print.css') }}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style type="text/css">
        .dash{
          border: 0 none;
          border-top: 2px dashed #322f32;
          background: none;
          height:0;
        } 
    </style>
    <title>{{ config('app.name') }}</title>
</head>
    <body>
       <table class="table-no-border" cellpadding="0.5" cellspacing="0.5">
            <tr>
                <td rowspan="4"><img height="80px" width="450px" src="{{ asset('upload/logo/'.$profil['gambar']) }}" /></td>
                <td width="35%" style="padding-left: 30px;"><b>PT GUMARA TRANS JAYA</b></td>
            </tr>
            <tr>
                <td style="padding-left: 30px;">Jl. Raya Semarang - Boja Km. 2</td>
            </tr>
            <tr>
                <td style="padding-left: 30px;">Tampingan Telp. (0294) 571 386</td>
            </tr>
            <tr>
                <td style="padding-left: 30px;">085 290 464 191 081 127 761 91</td>
            </tr>
        </table>
        <br>
        <center><h3>Laporan Pemasukan Tanggal Pemesanan {{ $dari }} s/d {{ $sampai }}</h3></center>
        @php
            $totalOrder = 0;
            $totalPengeluaran = 0;
            $totalPemasukan = 0;
        @endphp
        @foreach($listPemesanan as $pemesanan)
            <br>
            <table class="table-border" cellspacing="1" cellpadding="3">
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>No. Pemesanan</b></td>
                    <td colspan="3">{{ $pemesanan['no_pemesanan'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Bus</b></td>
                    <td colspan="3">
                        @foreach($pemesanan->pemesananBus as $busItem)
                            {{ $busItem->bus['merk'] }} / {{ $busItem->bus['plat_nomor'] }} <br>
                        @endforeach
                    </td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Nama Pemesan</b></td>
                    <td>{{ $pemesanan['nama'] }}</td>
                    <td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>No. HP</b></td>
                    <td width="150px">{{ $pemesanan['telepon'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>PJ Rombongan</b></td>
                    <td>{{ $pemesanan['pj_rombongan'] }}</td>
                    <td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>No. HP</b></td>
                    <td width="150px"></td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tanggal Keberangkatan</b></td>
                    <td>{{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_keberangkatan']))) }}</td>
                    <td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>Jam</b></td>
                    <td width="150px">{{ $pemesanan['jam_keberangkatan'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tanggal Kepulangan</b></td>
                    <td>{{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_kepulangan']))) }}</td>
                    <td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>Jam</b></td>
                    <td width="150px">{{ $pemesanan['jam_kepulangan'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tujuan</b></td>
                    <td colspan="3">{{ $pemesanan['tujuan'] }}</td>
                </tr>
                <tr>
                    <td width="170px" style="text-align: right; background-color: #c2c1c3;"><b>Alamat Penjemputan</b></td>
                    <td colspan="3">{{ $pemesanan['alamat_penjemputan'] }}</td>
                </tr>
            </table>
            <br>
            <b>Rincian Pengeluaran dan Pemasukan</b>
            <table class="table-border" cellspacing="1" cellpadding="3">
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Order</b></td>
                    <td colspan="3">Rp {{ $pemesanan['total_biaya'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Solar</b></td>
                    <td colspan="3">Rp {{ $pemesanan['solar'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Lain-Lain</b></td>
                    <td colspan="3">Rp {{ $pemesanan['pengeluaran_lain'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Uang Muka</b></td>
                    <td>Rp {{ $pemesanan['uang_muka'] }}</td>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Bayar Kekurangan</b></td>
                    <td>Rp {{ $pemesanan['bayar_kekurangan'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Gaji Sopir</b></td>
                    <td colspan>Rp {{ $pemesanan['sopir_rupiah'] }}</td>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Zakat Sopir ({{ $pemesanan['zakat_persen'] }}%)</b></td>
                    <td colspan>Rp {{ $pemesanan['sopir_zakat_rupiah'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Gaji Kernet</b></td>
                    <td colspan>Rp {{ $pemesanan['kernet_rupiah'] }}</td>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Zakat Sopir ({{ $pemesanan['zakat_persen'] }}%)</b></td>
                    <td colspan>Rp {{ $pemesanan['kernet_zakat_rupiah'] }}</td>
                </tr>
                <tr>
                    <td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Zakat Perusahaan ({{ $pemesanan['zakat_persen'] }}%)</b></td>
                    <td colspan="3">Rp {{ $pemesanan['pemasukan_zakat_rupiah'] }}</td>
                </tr>
                <tr>
                    <td width="170px" style="text-align: right; background-color: #c2c1c3;"><b>Potongan Kas</b></td>
                    <td colspan="3">Rp {{ $pemesanan['potongan_kas'] }}</td>
                </tr>
                <tr>
                    <td width="170px" style="text-align: right; background-color: #c2c1c3;"><b>Total Pemasukan</b></td>
                    <td colspan="3">Rp {{ $pemesanan['pemasukan'] }}</td>
                </tr>
            </table>
                @php
                    $totalOrder += hilangTitik($pemesanan['total_biaya']);
                    $totalPengeluaran += hilangTitik($pemesanan['solar']) + hilangTitik($pemesanan['pengeluaran_lain']) + hilangTitik($pemesanan['sopir_rupiah']) + hilangTitik($pemesanan['sopir_zakat_rupiah']) + hilangTitik($pemesanan['kernet_rupiah']) + hilangTitik($pemesanan['kernet_zakat_rupiah']) + hilangTitik($pemesanan['pemasukan_zakat_rupiah']) + hilangTitik($pemesanan['potongan_kas']);
                    $totalPemasukan += hilangTitik($pemesanan['pemasukan']);
                @endphp
            <br>
            <hr class="dash">
        @endforeach
            <br>
            <table class="table-border" cellspacing="1" cellpadding="3">
                <tr>
                    <td width="170px" height="30px" style="text-align: right; background-color: #c2c1c3;"><h3>Total Order</h3></td>
                    <td colspan="3"><h3>Rp {{ mataUang($totalOrder) }}</h3></td>
                </tr>
                <tr>
                    <td width="170px" height="30px" style="text-align: right; background-color: #c2c1c3;"><h3>Total Pengeluaran</h3></td>
                    <td colspan="3"><h3>Rp {{ mataUang($totalPengeluaran) }}</h3></td>
                </tr>
                <tr>
                    <td width="170px" height="30px" style="text-align: right; background-color: #c2c1c3;"><h3>Total Pemasukan</h3></td>
                    <td colspan="3"><h3>Rp {{ mataUang($totalPemasukan) }}</h3></td>
                </tr>
            </table>
    </body>
</html>
