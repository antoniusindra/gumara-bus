@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Laporan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Pengaturan</a></li>
                <li class="active">Laporan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="vtabs col-lg-12 col-sm-12 col-xs-12">
                            <ul class="nav tabs-vertical" style="width: 150px;">
                                <li class="tab nav-item active">
                                    <a data-toggle="tab" class="nav-link" href="#home3" aria-expanded="true"> <span class="visible-xs"><i class="ti-home"></i></span> <span class="hidden-xs">Laporan</span> </a>
                                </li>
                                <li class="tab nav-item">
                                    <a data-toggle="tab" class="nav-link" href="#profile3" aria-expanded="false"> <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Surat Jalan</span> </a>
                                </li>
                                <li class="tab nav-item">
                                    <a data-toggle="tab" class="nav-link" href="#jadwal3" aria-expanded="false"> <span class="visible-xs"><i class="ti-user"></i></span> <span class="hidden-xs">Jadwal Bus</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="home3" class="tab-pane active">
                                    {!! Form::open(['id'=>'form-laporan', 'method'=>'POST', 'route'=>'laporan.cetak-pemasukan', 'class'=>'form-horizontal', 'target'=>'_blank']) !!}
                                        <div class="form-group row">
                                            {!! Form::label('tanggal_pemesanan', 'Tgl. Pemesanan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-10">
                                                <div class="input-daterange input-group" id="date-range">
                                                    {!! Form::text('dari_tanggal', null, ['class'=>'form-control', 'placeholder'=>'Dari Tanggal']) !!}
                                                    <span class="input-group-addon bg-info b-0 text-white">s/d</span>
                                                    {!! Form::text('sampai_tanggal', null, ['class'=>'form-control', 'placeholder'=>'Dari Tanggal']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                                {!! Form::label('jenis_laporan', 'Jenis Laporan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('jenis_laporan', ['pemasukan'=>'Laporan Pemasukan'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih Jenis Laporan']) !!}
                                                </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group m-b-0">
                                                <div class="offset-sm-7 col-sm-5">
                                                    {!! Form::submit('Cetak', ['class'=>'btn btn-primary']) !!}
                                                    {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                                </div>
                                            </div>  
                                        </div> 
                                    {!! Form::close() !!}
                                </div>
                                <div id="profile3" class="tab-pane">
                                    {!! Form::open(['id'=>'form-surat-jalan', 'method'=>'POST', 'route'=>'pelunasan.cetak-surat-jalan', 'class'=>'form-horizontal', 'target'=>'_blank']) !!}
                                        <div class="form-group row">
                                            {!! Form::label('no_pemesanan', 'No. Pemesanan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-10">
                                                <div class="input-group">
                                                {!! Form::text('no_pemesanan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan No. Pemesanan', 'readonly'=>true]) !!}
                                                <span class="input-group-btn"> 
                                                    <button class="btn btn-info" type="button" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="loadData()">Cari Pemesanan</button> 
                                                </span>
                                                </div>
                                                {!! Form::hidden('id_pemesanan', null, ['class'=>'form-control', 'readonly'=>true, 'id'=>'id_pemesanan']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('nama', 'Nama Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama Pemesan', 'readonly'=>true]) !!}
                                            </div>

                                            {!! Form::label('telepon', 'Telp. Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. Pemesan', 'readonly'=>true]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('pj_rombongan', 'PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan PJ Rombongan', 'readonly'=>true]) !!}
                                            </div>

                                            {!! Form::label('telp_pj_rombongan', 'Telp. PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-4">
                                                {!! Form::text('telp_pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. PJ Rombongan', 'readonly'=>true]) !!}
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('user', 'User / Oleh', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::select('user', $var['user'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih User']) !!}
                                            </div>
                                        </div>

                                        <div class="form-group m-b-0">
                                            <div class="offset-sm-7 col-sm-5">
                                                {!! Form::submit('Cetak', ['class'=>'btn btn-primary']) !!}
                                                {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                            </div>
                                        </div>
                                    {!! Form::close() !!}  
                                </div>
                                <div id="jadwal3" class="tab-pane">
                                    {!! Form::open(['id'=>'form-jadwal', 'method'=>'POST', 'route'=>'laporan.jadwal-bus', 'class'=>'form-horizontal', 'target'=>'_blank']) !!}

                                        <div class="form-group row">
                                                {!! Form::label('bulan', 'Bulan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                                <div class="col-sm-10">
                                                    {!! Form::select('bulan', $var['bulan'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih Bulan']) !!}
                                                </div>
                                        </div>

                                        <div class="form-group row">
                                            {!! Form::label('tahun', 'Tahun', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                            <div class="col-sm-10">
                                                {!! Form::number('tahun', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Tahun']) !!}
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group m-b-0">
                                                <div class="offset-sm-7 col-sm-5">
                                                    {!! Form::submit('Cetak', ['class'=>'btn btn-primary']) !!}
                                                    {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                                </div>
                                            </div>  
                                        </div> 
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>

                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Daftar Pemesanan</h4> </div>
                <div class="modal-body">
                    <form method="GET" action="" id="formCari">
                        <div class="input-group">
                            <input name="cari" id="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="submit">Cari</button>
                            </span>
                        </div>
                    </form>
                    <br />

                    <div id="areaAKunModal"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function loadData(){
            $("#areaAKunModal").load('{!! url('/pelunasan/list-pemesanan-2') !!}');
        }

        $(document).ready(function() {  
            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                //console.log($(this).attr('href').split('page='));
                var page = $(this).attr('href').split('page=')[1];
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pelunasan/list-pemesanan-2?page=') }}'+page+'&cari='+cari,
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('submit', '#formCari', function (e) {
                e.preventDefault();
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pelunasan/list-pemesanan-2?') }}'+'cari='+cari
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('click', '#buttonPilih', function (e) {
                e.preventDefault();
                var id = $(this).attr('data-value');

                $.ajax({
                    method : 'post',
                    url : '{{ url('/pelunasan/list-pemesanan') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    // console.log(data);
                    $("#cari").val("");
                    $("#id_pemesanan").val(data.id);
                    $("#no_pemesanan").val(data.no_pemesanan);
                    $("#nama").val(data.nama);
                    $("#telepon").val(data.telepon);
                    $("#telp_pj_rombongan").val(data.telp_pj_rombongan);
                    $("#pj_rombongan").val(data.pj_rombongan);
                });
            });

            $("#form-laporan").validate({
                ignore: [],
                rules: {
                    dari_tanggal: "required",
                    sampai_tanggal: "required",
                    jenis_laporan: "required"
                },
                messages: {
                    dari_tanggal: "Kolom dari tanggal harus diisi",
                    sampai_tanggal: "Kolom sampai tanggal harus diisi",
                    jenis_laporan: "Kolom jenis laporan harus diisi",
                }
            });

            $("#form-surat-jalan").validate({
                ignore: [],
                rules: {
                    nama: "required",
                    telepon: "required",
                    pj_rombongan: "required",
                    telp_pj_rombongan: "required",
                    user: "required"
                },
                messages: {
                    nama: "Kolom nama harus diisi",
                    telepon: "Kolom telepon harus diisi",
                    pj_rombongan: "Kolom pj rombongan harus diisi",
                    telp_pj_rombongan: "Kolom telepon pj rombongan harus diisi",
                    user: "Kolom user harus diisi",
                }
            });

            $("#form-jadwal").validate({
                ignore: [],
                rules: {
                    bulan: "required",
                    tahun: "required"
                },
                messages: {
                    bulan: "Kolom bulan harus diisi",
                    tahun: "Kolom tahun harus diisi"
                }
            });
        });
    </script>
@endsection
