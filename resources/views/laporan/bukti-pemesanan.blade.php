<html>
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('elite/plugins/images/favicon.png') }}">
    <link href="{{ asset('elite/css/print.css') }}" rel="stylesheet">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }}</title>
</head>
	<body>
		<table class="table-no-border" cellpadding="0.5" cellspacing="0.5">
			<tr>
				<td rowspan="4"><img height="80px" width="450px" src="{{ asset('upload/logo/'.$profil['gambar']) }}" /></td>
				<td width="35%" style="padding-left: 30px;"><b>PT GUMARA TRANS JAYA</b></td>
			</tr>
			<tr>
				<td style="padding-left: 30px;">Jl. Raya Semarang - Boja Km. 2</td>
			</tr>
			<tr>
				<td style="padding-left: 30px;">Tampingan Telp. (0294) 571 386</td>
			</tr>
			<tr>
				<td style="padding-left: 30px;">085 290 464 191 081 127 761 91</td>
			</tr>
		</table>
		<br>
		<center><h3>Bukti Pemesanan</h3></center>
		<h4><b>Nomor : {{ $pemesanan['no_pemesanan'] }}</b></h4>

		<table class="table-border" cellspacing="1" cellpadding="3">
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Nama Pemesan</b></td>
				<td>{{ $pemesanan['nama'] }}</td>
				<td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>No. HP</b></td>
				<td width="150px">{{ $pemesanan['telepon'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>PJ Rombongan</b></td>
				<td>{{ $pemesanan['pj_rombongan'] }}</td>
				<td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>No. HP</b></td>
				<td width="150px">{{ $pemesanan['telp_pj_rombongan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tanggal Keberangkatan</b></td>
				<td>{{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_keberangkatan']))) }}</td>
				<td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>Jam</b></td>
				<td width="150px">{{ $pemesanan['jam_keberangkatan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tanggal Kepulangan</b></td>
				<td>{{ tanggal_format_indonesia(date('Y-m-d', strtotime($pemesanan['tanggal_kepulangan']))) }}</td>
				<td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>Jam</b></td>
				<td width="150px">{{ $pemesanan['jam_kepulangan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Tujuan</b></td>
				<td colspan="3">{{ $pemesanan['tujuan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Alamat Penjemputan</b></td>
				<td colspan="3">{{ $pemesanan['alamat_penjemputan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Jumlah Armada</b></td>
				<td>{{ $pemesanan['jumlah_armada'] }}</td>
				<td width="100px" style="text-align: right; background-color: #c2c1c3;"><b>Harga Satuan</b></td>
				<td width="150px">{{ $pemesanan['harga_satuan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Biaya Sewa</b></td>
				<td colspan="3">{{ $pemesanan['biaya_sewa'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Biaya Tambahan</b></td>
				<td colspan="3">{{ $pemesanan['biaya_tambahan'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Total Biaya</b></td>
				<td colspan="3">{{ $pemesanan['total_biaya'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Uang Muka</b></td>
				<td colspan="3">{{ $pemesanan['uang_muka'] }}</td>
			</tr>
			<tr>
				<td width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Sisa Pembayaran</b></td>
				<td colspan="3">{{ $pemesanan['sisa_pembayaran'] }}</td>
			</tr>
			<tr>
				<td height="80px" width="150px" style="text-align: right; background-color: #c2c1c3;"><b>Catatan</b></td>
				<td colspan="3">{{ $pemesanan['catatan'] }}</td>
			</tr>
		</table>
		<br>
		<table class="table-no-border">
			<tr>
				<td colspan="3" style="text-align:right; padding-right: 20px;"><b>Boja, {{ tanggal_format_indonesia(date('Y-m-d')) }}</b></td>
			</tr>
			<tr>
				<td style="text-align:center;" width="30%"><b>Pemesan</b></td>
				<td style="text-align:center;" width="40%"><b></b></td>
				<td style="text-align:center;" width="30%"><b>Gumara Transport</b></td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="3">&nbsp;</td>
			</tr>
			<tr>
				<td style="text-align:center;" width="30%"><b>( {{ $pemesanan['nama'] }} )</b></td>
				<td style="text-align:center;" width="40%"><b></b></td>
				<td style="text-align:center;" width="30%"><b>( {{ Auth::user()->name }} )</b></td>
			</tr>
		</table>
		<h5>SYARAT DAN KETENTUAN :</h5>
		<ul style="padding-top: -20px;">
		  	<li>Tarif di atas tidak termasuk biaya tol, parkir, pajak, retribusi penyeberangan / ferry</li>
		  	<li>Tarif dapat berubah sewaktu-waktu jika terjadi perubahan harga BBM</li>
		  	<li>Penyewa diharuskan membayar uang muka sebesar 25%</li>
		  	<li>Pelunasan dilakukan maksimal 3 hari sebelum keberangkatan</li>
		  	<li>Pembayaran dapat dilakukan langsung di kantor atau transfer melalui Bank BCA nomor rekening 0095350629 atas nama
		  		Arif Gumara Kumayan Jati</li>
		  	<li>Jika terjadi pembatalan, maka uang muka yang telah dibayarkan tidak dapat dikembalikan</li>
		  	<li>Penyewa berhak menegur pengemudi yang ugal-ugalan / melaporkan pada perusahaan</li>
		  	<li>Penyewa tidak diperbolehkan merubah / menambah rute perjalanan, diluar yang telah disepakati</li>
		  	<li>Kehilangan barang / tertukar bukan tanggung jawab crew / perusahaan</li>
		  	<li>Jika barang tertinggal / terbawa di dalam bus, silahkan menghubungi perusahaan</li>
		  	<li>Pengemudi berhak menolak jalan yang tidak memadai untuk dilalui bus</li>
		  	<li>Penyewa harus bertanggung jawab atas kerusakan yang disebabkan oleh peserta rombongan</li>
		</ul> 
	</body>
</html>
