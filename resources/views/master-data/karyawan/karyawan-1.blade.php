@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Karyawan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Master Data</a></li>
                <li class="active">Karyawan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active nav-item"><a href="{{ url('/karyawan') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="nav-item"><a href="{{ url('/karyawan/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-6">
                            <a href="{{ url('/karyawan/create') }}" class="btn btn-primary">Tambah Data</a>
                        </div>
                        <div class="col-md-6">
                            <form method="GET" action="">
                                <div class="input-group m-bot15">
                                    <input name="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">Cari</button>
                                    </span>
                                </div>
                            </form>
                            <br />
                        </div>
                        <div class="col-md-12"> 
                            <legend></legend>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table color-table inverse-table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="100px" style="text-align: center;"><b>Aksi</b></th>
                                            <th style="text-align: center;"><b>NIK</b></th>
                                            <th style="text-align: center;"><b>Nama</b></th>
                                            <th style="text-align: center;"><b>Email</b></th>
                                            <th style="text-align: center;"><b>No. HP</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 0;
                                        @endphp
                                        @foreach($listKaryawan as $item)
                                            @php
                                                $no++;
                                            @endphp
                                            <tr>
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'DELETE', 'route'=> ['karyawan.destroy', $item->id.$var['url']['all']], 'class'=> 'delete_form']) !!}
                                                    {!! Form::hidden('nomor', $no, ['class'=>'form-control']) !!}
                                                    <div class="btn-group" style="width: 100px;">
                                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                                        <a href="{{ url('/karyawan/'.$item->id.'/edit'.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        <a href="{{ url('/karyawan/'.$item->id.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                                <td>{{ $item['nik'] }}</td>
                                                <td>{{ $item['nama'] }}</td>
                                                <td>{{ $item['email'] }}</td>
                                                <td>{{ $item['no_hp'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-6 offset-md-6">
                             {{ $listKaryawan->render() }}
                        </div>
              
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
