@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Karyawan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Master Data</a></li>
                <li class="active">Karyawan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/karyawan') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/karyawan/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listKaryawan, ['method'=>'PATCH', 'route'=> ['karyawan.update', $listKaryawan->id.$var['url']['all']], 'id'=>'form-karyawan', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-karyawan', 'method'=>'POST', 'route'=>'karyawan.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listKaryawan, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('nik', 'NIK.', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('nik', null, ['class'=>'form-control', 'placeholder'=>'Inputkan NIK.']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('nama', 'Nama', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('alamat', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Alamat']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('no_hp', 'No. HP', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('no_hp', null, ['class'=>'form-control', 'placeholder'=>'Inputkan No. HP']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listKaryawan->id  }}';
            @else
                var pk = null;
            @endif

            $("#form-karyawan").validate({
                rules: {
                    nik: {
                        required: true,
                        remote: {
                            url: "{{ url('karyawan/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'nik',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    },
                    nama: "required",
                    email: {
                        email: true,
                        remote: {
                            url: "{{ url('karyawan/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'email',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    }
                },
                messages: {
                    nik: {
                        required: "Kolom nik harus diisi",
                        remote: "NIK sudah digunakan"
                    },
                    nama: "Kolom nama harus diisi",
                    email: {
                        email: "Format email salah",
                        remote: "NIK sudah digunakan"
                    }
                }
            });

        });
    </script>
@endsection
