@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Bus</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Master Data</a></li>
                <li class="active">Bus</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/bus') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/bus/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listBus, ['method'=>'PATCH', 'route'=> ['bus.update', $listBus->id.$var['url']['all']], 'id'=>'form-bus', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-bus', 'method'=>'POST', 'route'=>'bus.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listBus, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('kode', 'Kode', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('kode', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Kode']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('merk', 'Merk', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('merk', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Merk']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('plat_nomor', 'Plat Nomor', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('plat_nomor', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Plat Nomor']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('warna', 'Kode Warna', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::color('warna', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Warna']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listBus->id  }}';
            @else
                var pk = null;
            @endif

            $("#form-bus").validate({
                rules: {
                    kode: {
                        required: true,
                        remote: {
                            url: "{{ url('bus/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'kode',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    },
                    kode_warna: {
                        required: true,
                        remote: {
                            url: "{{ url('bus/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'kode_warna',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    }
                },
                messages: {
                    kode: {
                        required: "Kolom kode harus diisi",
                        remote: "Kode sudah digunakan"
                    },
                    kode_warna: {
                        required: "Kolom kode warna harus diisi",
                        remote: "Kode warna sudah digunakan"
                    }
                }
            });

        });
    </script>
@endsection
