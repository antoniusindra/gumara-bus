@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Ganti Password</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Ganti Password</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                                {!! Form::open(['id'=>'form-ganti-password', 'method'=>'POST', 'route'=>'ganti-password', 'class'=>'form-horizontal']) !!}

                                <div class="form-group row">
                                    {!! Form::label('password', 'Password Baru', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::password('password', ['class'=>'form-control', 'placeholder'=>'Inputkan Password Baru']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('password2', 'Konfirmasi Password', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::password('password2', ['class'=>'form-control', 'placeholder'=>'Inputkan Konfirmasi Password']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {  
            $("#form-ganti-password").validate({
                rules: {
                    password: "required",
                    password2: {
                        required: true,
                        equalTo: password
                    }
                },
                messages: {
                    password: "Kolom password harus diisi",
                    password2: {
                        required: "Kolom Konfirmasi password harus diisi",
                        equalTo: "Konfirmasi password harus sama dengan password baru"
                    }
                }
            });
        });
          
    </script>
@endsection
