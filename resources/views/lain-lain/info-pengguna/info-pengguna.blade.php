@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Info Pengguna</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Info Pengguna</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                                {!! Form::model($listUser, ['class'=>'form-horizontal']) !!}

                                <div class="form-group row">
                                    {!! Form::label('name', 'Nama', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('telepon', 'Telepon', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telepon']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('username', 'Username', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Username']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

