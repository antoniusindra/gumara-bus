@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">User</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Pengaturan</a></li>
                <li class="active">User</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/user') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/user/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listUser, ['method'=>'PATCH', 'route'=> ['user.update', $listUser->id.$var['url']['all']], 'id'=>'form-user', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-user', 'method'=>'POST', 'route'=>'user.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listUser, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('name', 'Nama', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('telepon', 'Telepon', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telepon']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('username', 'Username', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('username', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Username']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listUser->id  }}';
            @else
                var pk = null;
            @endif

            $("#form-user").validate({
                rules: {
                    name: "required",
                    email: {
                        email: true,
                        remote: {
                            url: "{{ url('user/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'email',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    },
                    telepon: {
                        number: true
                    },
                    username: {
                        required: true,
                        remote: {
                            url: "{{ url('user/cek-validasi') }}",
                            type: "post",
                            data: {
                                'kolom': 'username',
                                'aksi': '{{ $var['method'] }}',
                                'pk' : pk
                            }
                        }
                    }
                },
                messages: {
                    name: "Kolom nama harus diisi",
                    email: {
                        email: "Data harus sesuai dengan format email",
                        remote: "Email sudah digunakan"
                    },
                    telepon: {
                        number: "Hanya boleh disii dengan angka"
                    },
                    username: {
                        required: "Kolom username harus diisi",
                        remote: "Username sudah digunakan"
                    }
                }
            });

        });
    </script>
@endsection
