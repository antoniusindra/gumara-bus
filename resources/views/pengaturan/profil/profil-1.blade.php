@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Profil Perusahan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Pengaturan</a></li>
                <li class="active">Profil Perusahaan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        {!! Form::model($listProfil, ['method'=>'PATCH', 'route'=> ['profil-perusahaan.update', $listProfil->id], 'id'=>'form-profil', 'class'=>'form-horizontal', 'files'=>true]) !!}
                            <div class="col-md-9">
                                <div class="form-group row">
                                    {!! Form::label('nama', 'Nama Perusahaan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama Perusahaan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('alamat', 'Alamat', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('alamat', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Alamat']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('kota', 'Kota', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('kota', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Kota']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('email', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Email']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('telepon', 'Telepon', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telepon']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                {!! Form::label('gambar', 'Upload Logo Perusahaan .jpg/.png', ['class' => 'control-label']) !!}
                                {!! Form::file('gambar', ['class'=>'dropify', 'accept'=>'.jpg, .png',
                                        'data-default-file'=>(!empty($listProfil['gambar'])?URL('/').'/upload/logo/'.$listProfil->gambar:null) ]) !!}
                            </div>

                            <div class="col-md-12">
                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                </div>  
                            </div>
                            
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {      
            $('.dropify').dropify();

            $("#form-profil").validate({
                rules: {
                    nama: "required",
                },
                messages: {
                    nama: "Kolom nama harus diisi",
                }
            });
        });
    </script>
@endsection
