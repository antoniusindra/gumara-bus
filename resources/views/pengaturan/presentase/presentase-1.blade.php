@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Presentase</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li><a href="#">Pengaturan</a></li>
                <li class="active">Presentase</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        {!! Form::model($listPresentase, ['method'=>'PATCH', 'route'=> ['presentase.update', $listPresentase->id], 'id'=>'form-presentase', 'class'=>'form-horizontal', 'files'=>true]) !!}
                
                            <div class="form-group row">
                                {!! Form::label('zakat', 'Zakat', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        {!! Form::text('zakat', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase Zakat', 'onkeyup'=>'mataUang("zakat")']) !!}
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('gaji_sopir', 'Gaji Sopir', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                <div class="col-sm-10">
                                    <div class="input-group">
                                        {!! Form::text('gaji_sopir', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase Gaji Sopir', 'onkeyup'=>'mataUang("gaji_sopir")']) !!}
                                        <span class="input-group-addon">%</span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('gaji_kernet', 'Gaji Kernet', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                <div class="col-sm-10">
                                        <div class="input-group">
                                            {!! Form::text('gaji_kernet', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase Gaji Kernet', 'onkeyup'=>'mataUang("gaji_kernet")']) !!}
                                            <span class="input-group-addon">%</span>
                                        </div>
                                    </div>
                            </div>

                            <div class="form-group row">
                                {!! Form::label('potongan_kas', 'Potongan Kas', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                <div class="col-sm-10">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {!! Form::text('potongan_kas', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Potongan Kas', 'onkeyup'=>'mataUang("potongan_kas")']) !!}
                                            
                                        </div>
                                    </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                        {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                    </div>
                                </div>  
                            </div>
                            
                        {!! Form::close() !!}
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
