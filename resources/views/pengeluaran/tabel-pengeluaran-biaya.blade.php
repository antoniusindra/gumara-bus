<!-- /row -->
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table color-table inverse-table table-hover">
                <thead>
                    <tr>
                        <th width="30px" style="text-align: center;"><b>Hapus</b></th>
                        <th style="text-align: center;"><b>Keperluan</b></th>
                        <th style="text-align: center;"><b>Biaya</b></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=0;
                        $totalBiaya = 0;
                    @endphp
                    @foreach($listPengeluaranRincian as $item)     
                        <tr>
                            <td class="text-center">
                                @if($var['method']=='show')
                                    <a class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></a>
                                @else
                                    <a class="btn btn-danger btn-xs" onClick="hapusPengeluaranBiaya('{{ $no }}')"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </td>
                            <td>{{ $item['keperluan'] }}</td>
                            <td style="text-align: right;">{{ mataUang($item['biaya_rincian']) }}</td>
                        </tr>
                        @php
                            $no++;
                            $totalBiaya += $item['biaya_rincian'];
                        @endphp
                    @endforeach
                        <tr>
                            <td style="text-align: center;" colspan="2"><b>Total Keperluan Lain - Lain</b></td>
                            <td style="text-align: right;"><b><input type="text" name="totalBiayaLainLain" id="totalBiayaLainLain" class="form-control" value="{{ mataUang($totalBiaya) }}" readonly /></b></td>
                        </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.row -->