@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pengeluaran</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Pengeluaran</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active nav-item"><a href="{{ url('/pengeluaran') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="nav-item"><a href="{{ url('/pengeluaran/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-6">
                            <a href="{{ url('/pengeluaran/create') }}" class="btn btn-primary">Tambah Data</a>
                        </div>
                        <div class="col-md-6">
                            <form method="GET" action="">
                                <div class="input-group m-bot15">
                                    <input name="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">Cari</button>
                                    </span>
                                </div>
                            </form>
                            <br />
                        </div>
                        <div class="col-md-12"> 
                            <legend></legend>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table color-table inverse-table table-hover" style="width: 1500px;">
                                    <thead>
                                        <tr>
                                            <th width="130px" style="text-align: center;"><b>Aksi</b></th>
                                            <th style="text-align: center;"><b>No. Pemesanan</b></th>
                                            <th style="text-align: center;"><b>Nama</b></th>
                                            <th style="text-align: center;"><b>Telepon</b></th>
                                            <th style="text-align: center;"><b>Tgl Berangkat</b></th>
                                            <th style="text-align: center;"><b>Tgl Kepulangan</b></th>
                                            <th style="text-align: center;"><b>Tujuan</b></th>
                                            <th style="text-align: center;"><b>Status</b></th>
                                            <th style="text-align: center;"><b>Total Pengeluaran</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 0;
                                        @endphp
                                        @foreach($listPengeluaran as $item)
                                            @php
                                                $no++;
                                            @endphp
                                            <tr>
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'DELETE', 'route'=> ['pengeluaran.destroy', $item->id.$var['url']['all']], 'class'=> 'delete_form']) !!}
                                                    {!! Form::hidden('nomor', $no, ['class'=>'form-control']) !!}
                                                    <div class="btn-group" style="width: 130px;">
                                                        <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
                                                        <a href="{{ url('/pengeluaran/'.$item->id.'/edit'.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        <a href="{{ url('/pengeluaran/'.$item->id.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                                        <a href="{{ url('/pengeluaran/cetak/'.$item->id)}}" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-print"></i></a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                                <td>{{ $item['no_pemesanan'] }}</td>
                                                <td>{{ $item['nama'] }}</td>
                                                <td>{{ $item['telepon'] }}</td>
                                                <td>{{ $item['tanggal_keberangkatan'] }}</td>
                                                <td>{{ $item['tanggal_kepulangan'] }}</td>
                                                <td>{{ $item['tujuan'] }}</td>
                                                <td>{{ $item['status'] }}</td>
                                                <td style="text-align: right;">{{ $item['total_pengeluaran'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-6 offset-sm-6">
                             {{ $listPengeluaran->render() }}
                        </div>
              
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
