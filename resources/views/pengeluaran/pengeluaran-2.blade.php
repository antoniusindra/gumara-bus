@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pengeluaran</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Pengeluaran</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/pengeluaran') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/pengeluaran/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listPengeluaran, ['method'=>'PATCH', 'route'=> ['pengeluaran.update', $listPengeluaran->id.$var['url']['all']], 'id'=>'form-pengeluaran', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-pengeluaran', 'method'=>'POST', 'route'=>'pengeluaran.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listPengeluaran, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('no_pemesanan', 'No. Pemesanan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                        {!! Form::text('no_pemesanan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan No. Pemesanan', 'readonly'=>true]) !!}
                                        <span class="input-group-btn"> 
                                            <button class="btn btn-info" type="button" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="loadData()">Cari Pemesanan</button> 
                                        </span>
                                        </div>
                                        {!! Form::hidden('id_pemesanan', null, ['class'=>'form-control', 'readonly'=>true, 'id'=>'id_pemesanan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('nama', 'Nama Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama Pemesan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('telepon', 'Telp. Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. Pemesan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tujuan', 'Tujuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('tujuan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Tujuan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('alamat_penjemputan', 'Alamat Penjemputan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('alamat_penjemputan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Alamat Penjemputan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="offset-sm-2 col-sm-10">
                                        <div id="area_bus"></div>
                                    </div>
                                </div>

                                <legend>Biaya dan Pembayaran</legend>

                                <div class="form-group row">
                                    {!! Form::label('jumlah_armada', 'Jumlah Armada', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jumlah_armada', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Jumlah Armada', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('harga_satuan', 'Harga Satuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('harga_satuan', null, ['class'=>'form-control', 'readonly'=>true, 'placeholder'=>'Inputkan Harga Satuan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('biaya_sewa', 'Biaya Sewa', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_sewa', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Sewa', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('biaya_tambahan', 'Biaya Tambahan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_tambahan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Tambahan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('total_biaya', 'Total Biaya', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('total_biaya', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Total Biaya', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('uang_muka', 'Uang Muka', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('uang_muka', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Uang Muka', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('sisa_pembayaran', 'Sisa Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('sisa_pembayaran', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Sisa Pembayaran', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('bayar_kekurangan', 'Bayar Kekurangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('bayar_kekurangan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Kekurangan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('status', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Status', 'readonly'=>true, 'style'=>'font-weight:bold']) !!}
                                    </div>
                                </div>

                                <legend>Pengeluaran</legend>

                                <div class="form-group row">
                                    {!! Form::label('order', 'Order', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('order', (isset($listPengeluaran['total_biaya'])?$listPengeluaran['total_biaya']:null), ['class'=>'form-control', 'placeholder'=>'Inputkan Order', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('solar', 'Biaya Solar', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('solar', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Solar', 'onkeyup'=>'mataUang("solar"); totalPemasukan();']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('keperluan', 'Keperluan Lain - Lain', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-6">
                                        {!! Form::text('keperluan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Keperluan Lain - Lain']) !!}
                                    </div>

                                    {!! Form::label('biaya_rincian', 'Biaya', ['class' => 'col-sm-1 control-label col-form-label']) !!}
                                    <div class="col-sm-3">
                                        <div class="input-group">
                                            {!! Form::text('biaya_rincian', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya', 'onkeyup'=>'mataUang("biaya_rincian")']) !!}
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="buttonBiaya">Tambahkan</button> 
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="offset-sm-2 col-sm-10">
                                    <div id="area_biaya_pengeluaran"></div>
                                    {!! Form::hidden('totalBiayaLainLain2', (isset($listPengeluaran['pengeluaran_lain'])?$listPengeluaran['pengeluaran_lain']:null), ['class'=>'form-control', 'id'=>'totalBiayaLainLain2', 'readonly'=>true]) !!}
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('sopir_persen', 'Gaji Sopir', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            {!! Form::text('sopir_zakat_persen', (isset($listPengeluaran['zakat_persen'])?$listPengeluaran['zakat_persen']:null), ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase', 'id'=>'sopir_zakat_persen', 'onkeyup'=>'mataUang("sopir_zakat_persen"); totalPemasukan();']) !!}
                                            <span class="input-group-addon">% (Zakat)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('sopir_zakat_rupiah', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Zakat Sopir', 'id'=>'sopir_zakat_rupiah', 'readonly'=>true]) !!}
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            {!! Form::text('sopir_persen', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase', 'onkeyup'=>'mataUang("sopir_persen"); totalPemasukan();']) !!}
                                            <span class="input-group-addon">% (Gaji)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('sopir_rupiah', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Gaji Sopir', 'id'=>'sopir_rupiah', 'style'=>'font-weight:bold', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('kernet_persen', 'Gaji Kernet', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            {!! Form::text('kernet_zakat_persen', (isset($listPengeluaran['zakat_persen'])?$listPengeluaran['zakat_persen']:null), ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase', 'id'=>'kernet_zakat_persen', 'onkeyup'=>'mataUang("kernet_zakat_persen"); totalPemasukan();']) !!}
                                            <span class="input-group-addon">% (Zakat)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('kernet_zakat_rupiah', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Zakat Kernet', 'id'=>'kernet_zakat_rupiah', 'readonly'=>true]) !!}
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            {!! Form::text('kernet_persen', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase', 'onkeyup'=>'mataUang("kernet_persen"); totalPemasukan();']) !!}
                                            <span class="input-group-addon">% (Gaji)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('kernet_rupiah', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Gaji Kernet', 'id'=>'kernet_rupiah', 'style'=>'font-weight:bold', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('zakat_persen', 'Total Pemasukan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            {!! Form::text('zakat_persen', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Presentase', 'onkeyup'=>'mataUang("zakat_persen"); totalPemasukan();']) !!}
                                            <span class="input-group-addon">% (Zakat)</span>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('pemasukan_zakat_rupiah', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Zakat', 'id'=>'pemasukan_zakat_rupiah', 'readonly'=>true]) !!}
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <span class="input-group-addon">Rp</span>
                                            {!! Form::text('potongan_kas', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Potongan Kas', 'id'=>'potongan_kas', 'onkeyup'=>'mataUang("potongan_kas"); totalPemasukan();']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        {!! Form::text('pemasukan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Pemasukan', 'id'=>'pemasukan', 'readonly'=>true, 'style'=>'font-weight:bold;background:#00b4ff;']) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Daftar Pemesanan Lunas</h4> </div>
                <div class="modal-body">
                    <form method="GET" action="" id="formCari">
                        <div class="input-group">
                            <input name="cari" id="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="submit">Cari</button>
                            </span>
                        </div>
                    </form>
                    <br />

                    <div id="areaAKunModal"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function loadData(){
            $("#areaAKunModal").load('{!! url('/pengeluaran/list-pemesanan') !!}');
        }

        function hapusPengeluaranBiaya(id){
            swal({   
                reverseButton: true,
                title: "Data yakin dihapus ?",   
                text: "<b>Mohon diteliti sebelum menghapus data</b>",   
                type: "warning",  
                confirmButtonText: "Hapus",   
                cancelButtonText: "Batal",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",    
                closeOnConfirm: false,
                html: true
            }, function(){ 
               $.ajax({
                    method : 'post',
                    url : '{{ url('/pengeluaran/hapus-biaya-rincian') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    $("#area_biaya_pengeluaran").html(data);

                    var totalLainLain = $("#totalBiayaLainLain").val();
                    if(totalLainLain==""){totalLainLain=0;}
                    $("#totalBiayaLainLain2").val(totalLainLain);
                    totalPemasukan();

                    swal({
                        title: "Berhasil",  
                        type: "success",  
                        text: "<b>Data biaya berhasil dihapus</b>",   
                        html: true
                    });
                });
            }); 
        }

        function totalPemasukan(){
            var order = formatMataUang($("#order").val());
            var solar = formatMataUang($("#solar").val());
            var persenGajiSopir = formatMataUang($("#sopir_persen").val());
            var persenGajiKernet = formatMataUang($("#kernet_persen").val());
            var persenZakatSopir = formatMataUang($("#sopir_zakat_persen").val());
            var persenZakatKernet = formatMataUang($("#kernet_zakat_persen").val());
            var persenZakat = formatMataUang($("#zakat_persen").val());
            var totalBiayaLainLain = formatMataUang($("#totalBiayaLainLain2").val());
            var potonganKas = formatMataUang($("#potongan_kas").val());
            
            if(order==""){order=0;}
            if(solar==""){solar=0;}
            if(persenGajiSopir==""){persenGajiSopir=0;}
            if(persenGajiKernet==""){persenGajiKernet=0;}
            if(persenZakatSopir==""){persenZakatSopir=0;}
            if(persenZakatKernet==""){persenZakatKernet=0;}
            if(persenZakat==""){persenZakat=0;}
            if(totalBiayaLainLain==""){totalBiayaLainLain=0;}
            if(potonganKas==""){potonganKas=0;}

            var pemasukanPotongSolar = eval(order)-(eval(solar)+eval(totalBiayaLainLain));

            var sopir_zakat_rupiah = (eval(pemasukanPotongSolar)*(eval(persenGajiSopir)/100))*(eval(persenZakatSopir)/100);
            var sopir_gaji_rupiah = (eval(pemasukanPotongSolar)*(eval(persenGajiSopir)/100))-eval(sopir_zakat_rupiah);
            $('#sopir_zakat_rupiah').val(mataUang2(sopir_zakat_rupiah));
            $('#sopir_rupiah').val(mataUang2(sopir_gaji_rupiah));

            var kernet_zakat_rupiah = (eval(pemasukanPotongSolar)*(eval(persenGajiKernet)/100))*(eval(persenZakatKernet)/100);
            var kernet_gaji_rupiah = (eval(pemasukanPotongSolar)*(eval(persenGajiKernet)/100))-eval(kernet_zakat_rupiah);
            $('#kernet_zakat_rupiah').val(mataUang2(kernet_zakat_rupiah));
            $('#kernet_rupiah').val(mataUang2(kernet_gaji_rupiah));
            
            var gaji_sopir = eval(pemasukanPotongSolar)*(eval(persenGajiSopir)/100);
            var gaji_kernet = eval(pemasukanPotongSolar)*(eval(persenGajiKernet)/100);
            var pemasukan_zakat = (eval(pemasukanPotongSolar)-eval(gaji_sopir)-eval(gaji_kernet))*(eval(persenZakat)/100);
            var pemasukan = eval(pemasukanPotongSolar)-eval(gaji_sopir)-eval(gaji_kernet)-eval(pemasukan_zakat);
            var pemasukanPotongKas = eval(pemasukan)-eval(potonganKas);
            $('#pemasukan_zakat_rupiah').val(mataUang2(pemasukan_zakat));
            $('#pemasukan').val(mataUang2(pemasukanPotongKas));
        }

        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listPengeluaran->id  }}';
                 $("#area_bus").load('{!! url('/pengeluaran/bus2') !!}?method={{ $var['method'] }}&id={{ $listPengeluaran->id }}');
                 $("#area_biaya_pengeluaran").load('{!! url('/pengeluaran/biaya-rincian2') !!}?method={{ $var['method'] }}&id={{ $listPengeluaran->id }}');
            @else
                var pk = null;
            @endif

            $("#form-pengeluaran").validate({
                rules: {
                    nama: "required"
                },
                messages: {
                    nama: "Kolom nama harus diisi"
                }
            });

            $("#buttonBiaya").on("click", function () {
                var keperluan = $("#keperluan").val();
                var biaya = formatMataUang($("#biaya_rincian").val());

                if(keperluan != "" && biaya != ""){
                    $.ajax({
                        method : 'post',
                        url : '{{ url('/pengeluaran/biaya-rincian') }}',
                        data : 'keperluan='+keperluan+'&biaya='+biaya,
                    }).success(function (data) {
                        $("#area_biaya_pengeluaran").html(data);
                        $("#keperluan").val("");
                        $("#biaya_rincian").val("");

                        var totalLainLain = $("#totalBiayaLainLain").val();
                        if(totalLainLain==""){totalLainLain=0;}
                        $("#totalBiayaLainLain2").val(totalLainLain);
                        totalPemasukan();
                    });
                }else{
                    swal({
                        title: "Opps !",   
                        text: "<b>Data keperluan/biaya masih kosong</b>",   
                        type: "error", 
                        html: true
                    });
                }
            });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                //console.log($(this).attr('href').split('page='));
                var page = $(this).attr('href').split('page=')[1];
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pengeluaran/list-pemesanan?page=') }}'+page+'&cari='+cari,
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('submit', '#formCari', function (e) {
                e.preventDefault();
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pengeluaran/list-pemesanan?') }}'+'cari='+cari
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('click', '#buttonPilih', function (e) {
                e.preventDefault();
                var id = $(this).attr('data-value');

                $.ajax({
                    method : 'post',
                    url : '{{ url('/pengeluaran/list-pemesanan') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    console.log(data);
                    $("#cari").val("");
                    $("#id_pemesanan").val(data.id);
                    $("#no_pemesanan").val(data.no_pemesanan);
                    $("#nama").val(data.nama);
                    $("#telepon").val(data.telepon);
                    $("#tujuan").val(data.tujuan);
                    $("#alamat_penjemputan").val(data.alamat_penjemputan);

                    $("#area_bus").load('{!! url('/pengeluaran/bus2') !!}?id='+data.id);
                    $("#jumlah_armada").val(data.jumlah_armada);
                    $("#harga_satuan").val(data.harga_satuan);
                    $("#biaya_sewa").val(data.biaya_sewa);
                    $("#biaya_tambahan").val(data.biaya_tambahan);
                    $("#total_biaya").val(data.total_biaya);
                    $("#uang_muka").val(data.uang_muka);
                    $("#sisa_pembayaran").val(data.sisa_pembayaran);
                    $("#bayar_kekurangan").val(data.bayar_kekurangan);
                    $("#status").val(data.status);

                    $("#order").val(data.total_biaya);

                    $("#sopir_zakat_persen").val(data.zakat_persen);
                    $("#sopir_zakat_rupiah").val(data.sopir_zakat_rupiah);
                    $("#sopir_persen").val(data.sopir_persen);
                    $("#sopir_rupiah").val(data.sopir_gaji_rupiah);

                    $("#kernet_zakat_persen").val(data.zakat_persen);
                    $("#kernet_zakat_rupiah").val(data.kernet_zakat_rupiah);
                    $("#kernet_persen").val(data.kernet_persen);
                    $("#kernet_rupiah").val(data.kernet_gaji_rupiah);

                    $("#zakat_persen").val(data.zakat_persen);
                    $("#pemasukan_zakat_rupiah").val(data.pemasukan_zakat_rupiah);
                    $("#potongan_kas").val(data.potongan_kas);
                    $("#pemasukan").val(data.pemasukan_rupiah);
                });
            });
        });
    </script>
@endsection
