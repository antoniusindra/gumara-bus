<!-- /row -->
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table color-table inverse-table table-hover" style="width: 900px;">
                <thead>
                    <tr>
                        <th width="30px" style="text-align: center;"><b>Aksi</b></th>
                        <th style="text-align: center;"><b>No. Pemesanan</b></th>
                        <th style="text-align: center;"><b>Nama</b></th>
                        <th style="text-align: center;"><b>Telepon</b></th>
                        <th style="text-align: center;"><b>Tujuan</b></th>
                        <th style="text-align: center;"><b>Tgl Berangkat</b></th>
                        <th style="text-align: center;"><b>Tgl Pulang</b></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($listPemesanan as $item)     
                    <tr>
                        <td class="text-center">
                            <a href="" class="btn btn-primary btn-sm" id="buttonPilih" data-dismiss="modal" data-value="{{ $item->id }}"><i class="fa fa-plus"></i></a>
                        </td>
                        <td>{{ $item['no_pemesanan'] }}</td>
                        <td>{{ $item['nama'] }}</td>
                        <td>{{ $item['telepon'] }}</td>
                        <td>{{ $item['tujuan'] }}</td>
                        <td>{{ $item['tanggal_keberangkatan'] }}</td>
                        <td>{{ $item['tanggal_kepulangan'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-md-6 offset-md-6">
         {{ $listPemesanan->render() }}
    </div>
</div>
<!-- /.row -->