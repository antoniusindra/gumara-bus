@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pelunasan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Pelunasan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/pelunasan') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/pelunasan/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listPelunasan, ['method'=>'PATCH', 'route'=> ['pelunasan.update', $listPelunasan->id.$var['url']['all']], 'id'=>'form-pelunasan', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-pelunasan', 'method'=>'POST', 'route'=>'pelunasan.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listPelunasan, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('no_pemesanan', 'No. Pemesanan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                        {!! Form::text('no_pemesanan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan No. Pemesanan', 'readonly'=>true]) !!}
                                        <span class="input-group-btn"> 
                                            <button class="btn btn-info" type="button" alt="default" data-toggle="modal" data-target=".bs-example-modal-lg" onclick="loadData()">Cari Pemesanan</button> 
                                        </span>
                                        </div>
                                        {!! Form::hidden('id_pemesanan', null, ['class'=>'form-control', 'readonly'=>true, 'id'=>'id_pemesanan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('nama', 'Nama Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama Pemesan', 'readonly'=>true]) !!}
                                    </div>

                                    {!! Form::label('telepon', 'Telp. Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. Pemesan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('pj_rombongan', 'PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan PJ Rombongan', 'readonly'=>true]) !!}
                                    </div>

                                    {!! Form::label('telp_pj_rombongan', 'Telp. PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('telp_pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. PJ Rombongan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tanggal_keberangkatan', 'Tgl. Keberangkatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('tanggal_keberangkatan', null, ['class'=>'form-control datepicker-autoclose', 'placeholder'=>'Inputkan Tgl. Keberangkatan', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('jam_keberangkatan', 'Jam Keberangkatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jam_keberangkatan', null, ['class'=>'form-control clockpicker', 'placeholder'=>'Inputkan Jam Keberangkatan', 'data-autoclose'=>true, 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tanggal_kepulangan', 'Tgl. Kepulangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('tanggal_kepulangan', null, ['class'=>'form-control datepicker-autoclose', 'placeholder'=>'Inputkan Tgl. Kepulangan', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('jam_kepulangan', 'Jam Kepulangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jam_kepulangan', null, ['class'=>'form-control clockpicker', 'placeholder'=>'Inputkan Jam Kepulangan', 'data-autoclose'=>true, 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tujuan', 'Tujuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('tujuan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Tujuan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('alamat_penjemputan', 'Alamat Penjemputan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('alamat_penjemputan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Alamat Penjemputan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('bus', 'Bus', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            {!! Form::select('bus', $var['bus'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih Bus', 'disabled'=>true]) !!}
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info disabled" type="button" id="buttonBus">Tambahkan</button> 
                                            </span>
                                        </div>
                                        <br />
                                        <div id="area_bus"></div>
                                    </div>
                                </div>

                                <legend>Biaya dan Pembayaran</legend>

                                <div class="form-group row">
                                    {!! Form::label('jumlah_armada', 'Jumlah Armada', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jumlah_armada', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Jumlah Armada', 'onkeyup'=>'jumlahPembayaran()', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('harga_satuan', 'Harga Satuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('harga_satuan', null, ['class'=>'form-control', 'onkeyup'=>'mataUang("harga_satuan"); jumlahPembayaran();', 'placeholder'=>'Inputkan Harga Satuan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('biaya_sewa', 'Biaya Sewa', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_sewa', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Sewa', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('biaya_tambahan', 'Biaya Tambahan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_tambahan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Tambahan', 'onkeyup'=>'mataUang("biaya_tambahan"); jumlahPembayaran();']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('total_biaya', 'Total Biaya', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('total_biaya', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Total Biaya', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('uang_muka', 'Uang Muka', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('uang_muka', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Uang Muka', 'onkeyup'=>'mataUang("uang_muka"); jumlahPembayaran();', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('sisa_pembayaran', 'Sisa Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('sisa_pembayaran', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Sisa Pembayaran', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('pembayaran', 'Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::select('pembayaran', array('Tunai' => 'Tunai', 'Transfer' => 'Transfer'), null, ['class'=>'form-control', 'disabled'=>true]) !!}
                                    </div>
                                    {!! Form::label('bank', 'Bank', ['class' => 'col-sm-1 control-label col-form-label']) !!}
                                    <div class="col-sm-5">
                                        {!! Form::select('bank', array('BCA' => 'BCA'), null, ['class'=>'form-control', 'placeholder'=>'Pilih Bank', 'disabled'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('catatan', 'Catatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('catatan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Catatan', 'rows'=>5, 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <legend>Pelunasan</legend>

                                <div class="form-group row">
                                    {!! Form::label('bayar_kekurangan', 'Bayar Kekurangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('bayar_kekurangan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Kekurangan', 'onkeyup'=>'mataUang("bayar_kekurangan"); jumlahPembayaran();']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('status', 'Status', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('status', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Status', 'readonly'=>true, 'style'=>'font-weight:bold']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('pembayaran_kekurangan', 'Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::select('pembayaran_kekurangan', array('Tunai' => 'Tunai', 'Transfer' => 'Transfer'), null, ['class'=>'form-control']) !!}
                                    </div>
                                    {!! Form::label('bank_kekurangan', 'Bank', ['class' => 'col-sm-1 control-label col-form-label']) !!}
                                    <div class="col-sm-5">
                                        {!! Form::select('bank_kekurangan', array('BCA' => 'BCA'), null, ['class'=>'form-control', 'placeholder'=>'Pilih Bank']) !!}
                                    </div>
                                </div>

                                {{-- <div class="form-group row">
                                    {!! Form::label('user', 'User / Oleh', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::select('user', $var['user'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih User']) !!}
                                    </div>
                                </div> --}}

                                <div class="form-group row">
                                    {!! Form::label('keterangan', 'Keterangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('keterangan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Keterangan', 'rows'=>5]) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myLargeModalLabel">Daftar Pemesanan</h4> </div>
                <div class="modal-body">
                    <form method="GET" action="" id="formCari">
                        <div class="input-group">
                            <input name="cari" id="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                            <span class="input-group-btn">
                                <button class="btn btn-info" type="submit">Cari</button>
                            </span>
                        </div>
                    </form>
                    <br />

                    <div id="areaAKunModal"></div>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function loadData(){
            $("#areaAKunModal").load('{!! url('/pelunasan/list-pemesanan') !!}');
        }

        function jumlahPembayaran(){
            var jumlahArmada = formatMataUang($("#jumlah_armada").val());
            var hargaSatuan = formatMataUang($("#harga_satuan").val());
            var biayaTambahan = formatMataUang($("#biaya_tambahan").val());
            var uangMuka = formatMataUang($("#uang_muka").val());
            var bayarKekurangan = formatMataUang($("#bayar_kekurangan").val());
            var status;

            if(jumlahArmada==""){jumlahArmada=0;}
            if(hargaSatuan==""){hargaSatuan=0;}
            if(biayaTambahan==""){biayaTambahan=0;}
            if(uangMuka==""){uangMuka=0;}
            if(bayarKekurangan==""){bayarKekurangan=0;}

            var biayaSewa = eval(jumlahArmada)*eval(hargaSatuan);
            $('#biaya_sewa').val(mataUang2(biayaSewa));

            var totalBiaya = eval(biayaSewa)+eval(biayaTambahan);
            $('#total_biaya').val(mataUang2(totalBiaya));

            var sisaPembayaran = eval(totalBiaya)-(eval(uangMuka)+eval(bayarKekurangan));
            $('#sisa_pembayaran').val(mataUang2(sisaPembayaran));

            if(sisaPembayaran == 0){
                status = "Lunas";
            }else if(sisaPembayaran > 0 && uangMuka > 0){
                status = "Dipesan Sudah DP";
            }else if(uangMuka == 0){
                status = "Dipesan Tanpa DP";
            }

            $("#status").val(status);
        }

        function hapusPemesananBus(id){
            swal({   
                reverseButton: true,
                title: "Data yakin dihapus ?",   
                text: "<b>Mohon diteliti sebelum menghapus data</b>",   
                type: "warning",  
                confirmButtonText: "Hapus",   
                cancelButtonText: "Batal",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",    
                closeOnConfirm: false,
                html: true
            }, function(){ 
               $.ajax({
                    method : 'post',
                    url : '{{ url('/pelunasan/hapus-bus') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    $("#area_bus").html(data);
                    swal({
                        title: "Berhasil",  
                        type: "success",  
                        text: "<b>Data bus berhasil dihapus</b>",   
                        html: true
                    });
                });
            }); 
        }

        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listPelunasan->id  }}';
                 $("#area_bus").load('{!! url('/pelunasan/bus2') !!}?method={{ $var['method'] }}&id={{ $listPelunasan->id }}');
            @else
                var pk = null;
            @endif

            $("#form-pelunasan").validate({
                rules: {
                    nama: "required",
                    telepon: "required",
                    tanggal_keberangkatan: "required",
                    jam_keberangkatan: "required",
                    tanggal_kepulangan: "required",
                    jam_kepulangan: "required",
                    tujuan: "required",
                    alamat_penjemputan: "required"
                },
                messages: {
                    nama: "Kolom nama harus diisi",
                    telepon: "Kolom telepon harus diisi",
                    tanggal_keberangkatan: "Kolom tanggal keberangkatan harus diisi",
                    jam_keberangkatan: "Kolom jam keberangkatan harus diisi",
                    tanggal_kepulangan: "Kolom tanggal kepulangan harus diisi",
                    jam_kepulangan: "Kolom jam kepulangan harus diisi",
                    tujuan: "Kolom tujuan harus diisi",
                    alamat_penjemputan: "Kolom alamat penjemputan harus diisi"
                }
            });

            // $("#buttonBus").on("click", function () {
            //     var bus = $("#bus").val();
            //     var tglBerangkat = $("#tanggal_keberangkatan").val();
            //     var tglKepulangan = $("#tanggal_kepulangan").val();

            //     if(bus != "" && tglBerangkat != "" && tglKepulangan != ""){
            //         $.ajax({
            //             method : 'post',
            //             url : '{{ url('/pelunasan/bus') }}',
            //             data : 'bus='+bus+'&tglBerangkat='+tglBerangkat+'&tglKepulangan='+tglKepulangan,
            //         }).success(function (data) {
            //             $("#area_bus").html(data);
            //             $("#bus").select2('val','');
            //         });
            //     }else{
            //         swal({
            //             title: "Opps !",   
            //             text: "<b>Data bus/tgl berangkat/tgl kepulangan masih kosong</b>",   
            //             type: "error", 
            //             html: true
            //         });
            //     }
            // });

            $(document).on('click', '.pagination a', function (e) {
                e.preventDefault();
                //console.log($(this).attr('href').split('page='));
                var page = $(this).attr('href').split('page=')[1];
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pelunasan/list-pemesanan?page=') }}'+page+'&cari='+cari,
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('submit', '#formCari', function (e) {
                e.preventDefault();
                var cari = $('#cari').val();

                $.ajax({
                    url : '{{ url('/pelunasan/list-pemesanan?') }}'+'cari='+cari
                }).success(function (data) {
                    //console.log(data);
                    $('#areaAKunModal').html(data);
                });
            });

            $(document).on('click', '#buttonPilih', function (e) {
                e.preventDefault();
                var id = $(this).attr('data-value');

                $.ajax({
                    method : 'post',
                    url : '{{ url('/pelunasan/list-pemesanan') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    // console.log(data);
                    $("#cari").val("");
                    $("#id_pemesanan").val(data.id);
                    $("#no_pemesanan").val(data.no_pemesanan);
                    $("#nama").val(data.nama);
                    $("#telepon").val(data.telepon);
                    $("#telp_pj_rombongan").val(data.telp_pj_rombongan);
                    $("#pj_rombongan").val(data.pj_rombongan);
                    $("#tanggal_keberangkatan").val(data.tanggal_keberangkatan);
                    $("#jam_keberangkatan").val(data.jam_keberangkatan);
                    $("#tanggal_kepulangan").val(data.tanggal_kepulangan);
                    $("#jam_kepulangan").val(data.jam_kepulangan);
                    $("#tujuan").val(data.tujuan);
                    $("#alamat_penjemputan").val(data.alamat_penjemputan);

                    $("#area_bus").load('{!! url('/pelunasan/bus2') !!}?id='+data.id);
                    $("#jumlah_armada").val(data.jumlah_armada);
                    $("#harga_satuan").val(data.harga_satuan);
                    $("#biaya_sewa").val(data.biaya_sewa);
                    $("#biaya_tambahan").val(data.biaya_tambahan);
                    $("#total_biaya").val(data.total_biaya);
                    $("#uang_muka").val(data.uang_muka);
                    $("#sisa_pembayaran").val(data.sisa_pembayaran);
                    $("#pembayaran").val(data.pembayaran);
                    $("#bank").val(data.bank);
                    $("#catatan").val(data.catatan);
                    //pelunasan
                    $("#bayar_kekurangan").val(data.sisa_pembayaran);
                    $("#status").val("Lunas");
                });
            });
        });
    </script>
@endsection
