@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pemesanan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Pemesanan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item"><a href="{{ url('/pemesanan') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="active nav-item"><a href="{{ url('/pemesanan/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-12">

                            @if($var['method']=='edit')
                                {!! Form::model($listPemesanan, ['method'=>'PATCH', 'route'=> ['pemesanan.update', $listPemesanan->id.$var['url']['all']], 'id'=>'form-pemesanan', 'class'=>'form-horizontal']) !!}
                            @elseif($var['method']=='create')
                                {!! Form::open(['id'=>'form-pemesanan', 'method'=>'POST', 'route'=>'pemesanan.store', 'class'=>'form-horizontal']) !!}
                            @else
                                {!! Form::model($listPemesanan, ['class'=>'form-horizontal']) !!}
                            @endif

                                <div class="form-group row">
                                    {!! Form::label('no_pemesanan', 'No. Pemesanan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('no_pemesanan', ($var['method']=='create'?$var['no_pemesanan']:null), ['class'=>'form-control', 'placeholder'=>'Inputkan No. Pemesanan', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('nama', 'Nama Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('nama', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Nama Pemesan']) !!}
                                    </div>
                                    {!! Form::label('telepon', 'Telp. Pemesan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('telepon', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. Pemesan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('pj_rombongan', 'PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan PJ Rombongan']) !!}
                                    </div>
                                    {!! Form::label('telp_pj_rombongan', 'Telp. PJ Rombongan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('telp_pj_rombongan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Telp. PJ Rombongan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tanggal_keberangkatan', 'Tgl. Keberangkatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('tanggal_keberangkatan', null, ['class'=>'form-control datepicker-autoclose', 'placeholder'=>'Inputkan Tgl. Keberangkatan']) !!}
                                    </div>
                                    {!! Form::label('jam_keberangkatan', 'Jam Keberangkatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jam_keberangkatan', null, ['class'=>'form-control clockpicker', 'placeholder'=>'Inputkan Jam Keberangkatan', 'data-autoclose'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tanggal_kepulangan', 'Tgl. Kepulangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('tanggal_kepulangan', null, ['class'=>'form-control datepicker-autoclose', 'placeholder'=>'Inputkan Tgl. Kepulangan']) !!}
                                    </div>
                                    {!! Form::label('jam_kepulangan', 'Jam Kepulangan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jam_kepulangan', null, ['class'=>'form-control clockpicker', 'placeholder'=>'Inputkan Jam Kepulangan', 'data-autoclose'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('tujuan', 'Tujuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('tujuan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Tujuan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('alamat_penjemputan', 'Alamat Penjemputan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('alamat_penjemputan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Alamat Penjemputan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('bus', 'Bus', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        <div class="input-group">
                                            {!! Form::select('bus', $var['bus'], null, ['class'=>'form-control select2', 'placeholder'=>'Pilih Bus']) !!}
                                            <span class="input-group-btn"> 
                                                <button class="btn btn-info" type="button" id="buttonBus">Tambahkan</button> 
                                            </span>
                                        </div>
                                        <br />
                                        <div id="area_bus"></div>
                                    </div>
                                </div>

                                <legend>Biaya dan Pembayaran</legend>

                                <div class="form-group row">
                                    {!! Form::label('jumlah_armada', 'Jumlah Armada', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('jumlah_armada', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Jumlah Armada', 'onkeyup'=>'jumlahPembayaran()']) !!}
                                    </div>
                                    {!! Form::label('harga_satuan', 'Harga Satuan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('harga_satuan', null, ['class'=>'form-control', 'onkeyup'=>'mataUang("harga_satuan"); jumlahPembayaran();', 'placeholder'=>'Inputkan Harga Satuan']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('biaya_sewa', 'Biaya Sewa', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_sewa', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Sewa', 'readonly'=>true]) !!}
                                    </div>
                                    {!! Form::label('biaya_tambahan', 'Biaya Tambahan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::text('biaya_tambahan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Biaya Tambahan', 'onkeyup'=>'mataUang("biaya_tambahan"); jumlahPembayaran();']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('total_biaya', 'Total Biaya', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('total_biaya', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Total Biaya', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('uang_muka', 'Uang Muka', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('uang_muka', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Uang Muka', 'onkeyup'=>'mataUang("uang_muka"); jumlahPembayaran();']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('sisa_pembayaran', 'Sisa Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::text('sisa_pembayaran', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Sisa Pembayaran', 'readonly'=>true]) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('pembayaran', 'Pembayaran', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-4">
                                        {!! Form::select('pembayaran', array('Tunai' => 'Tunai', 'Transfer' => 'Transfer'), null, ['class'=>'form-control']) !!}
                                    </div>
                                    {!! Form::label('bank', 'Bank', ['class' => 'col-sm-1 control-label col-form-label']) !!}
                                    <div class="col-sm-5">
                                        {!! Form::select('bank', array('BCA' => 'BCA'), null, ['class'=>'form-control', 'placeholder'=>'Pilih Bank']) !!}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    {!! Form::label('catatan', 'Catatan', ['class' => 'col-sm-2 control-label col-form-label']) !!}
                                    <div class="col-sm-10">
                                        {!! Form::textarea('catatan', null, ['class'=>'form-control', 'placeholder'=>'Inputkan Catatan', 'rows'=>5]) !!}
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="offset-sm-7 col-sm-5">
                                        @if($var['method']=='edit')
                                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @elseif($var['method']=='create')
                                            {!! Form::submit('Simpan', ['class'=>'btn btn-primary']) !!}
                                            {!! Form::reset('Reset', ['class'=>'btn btn-danger']) !!}
                                        @else
                                            <a href="{{ url()->previous() }}" class="btn btn-primary">Kembali</a>
                                        @endif
                                    </div>
                                </div>

                            {!! Form::close() !!}  
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        function jumlahPembayaran(){
            var jumlahArmada = formatMataUang($("#jumlah_armada").val());
            var hargaSatuan = formatMataUang($("#harga_satuan").val());
            var biayaTambahan = formatMataUang($("#biaya_tambahan").val());
            var uangMuka = formatMataUang($("#uang_muka").val());

            if(jumlahArmada==""){jumlahArmada=0;}
            if(hargaSatuan==""){hargaSatuan=0;}
            if(biayaTambahan==""){biayaTambahan=0;}
            if(uangMuka==""){uangMuka=0;}

            var biayaSewa = eval(jumlahArmada)*eval(hargaSatuan);
            $('#biaya_sewa').val(mataUang2(biayaSewa));

            var totalBiaya = eval(biayaSewa)+eval(biayaTambahan);
            $('#total_biaya').val(mataUang2(totalBiaya));

            var sisaPembayaran = eval(totalBiaya)-eval(uangMuka);
            $('#sisa_pembayaran').val(mataUang2(sisaPembayaran));
        }

        function hapusPemesananBus(id){
            swal({   
                reverseButton: true,
                title: "Data yakin dihapus ?",   
                text: "<b>Mohon diteliti sebelum menghapus data</b>",   
                type: "warning",  
                confirmButtonText: "Hapus",   
                cancelButtonText: "Batal",   
                showCancelButton: true,   
                confirmButtonColor: "#DD6B55",    
                closeOnConfirm: false,
                html: true
            }, function(){ 
               $.ajax({
                    method : 'post',
                    url : '{{ url('/pemesanan/hapus-bus') }}',
                    data : 'id='+id,
                }).success(function (data) {
                    $("#area_bus").html(data);
                    swal({
                        title: "Berhasil",  
                        type: "success",  
                        text: "<b>Data bus berhasil dihapus</b>",   
                        html: true
                    });
                });
            }); 
        }

        $(document).ready(function() {  
            @if($var['method']=='edit' || $var['method']=='show')
                var pk = '{{ $listPemesanan->id  }}';
                 $("#area_bus").load('{!! url('/pemesanan/bus2') !!}?method={{ $var['method'] }}&id={{ $listPemesanan->id }}');
            @else
                var pk = null;
            @endif

            $("#form-pemesanan").validate({
                rules: {
                    nama: "required",
                    telepon: "required",
                    tanggal_keberangkatan: "required",
                    jam_keberangkatan: "required",
                    tanggal_kepulangan: "required",
                    jam_kepulangan: "required",
                    tujuan: "required",
                    alamat_penjemputan: "required"
                },
                messages: {
                    nama: "Kolom nama harus diisi",
                    telepon: "Kolom telepon harus diisi",
                    tanggal_keberangkatan: "Kolom tanggal keberangkatan harus diisi",
                    jam_keberangkatan: "Kolom jam keberangkatan harus diisi",
                    tanggal_kepulangan: "Kolom tanggal kepulangan harus diisi",
                    jam_kepulangan: "Kolom jam kepulangan harus diisi",
                    tujuan: "Kolom tujuan harus diisi",
                    alamat_penjemputan: "Kolom alamat penjemputan harus diisi"
                }
            });

            $("#buttonBus").on("click", function () {
                var bus = $("#bus").val();
                var tglBerangkat = $("#tanggal_keberangkatan").val();
                var tglKepulangan = $("#tanggal_kepulangan").val();
                var listBus = [];

                if(bus != "" && tglBerangkat != "" && tglKepulangan != ""){
                    $.ajax({
                        method : 'post',
                        url : '{{ url('/pemesanan/bus') }}',
                        data : 'bus='+bus+'&tglBerangkat='+tglBerangkat+'&tglKepulangan='+tglKepulangan,
                    }).success(function (data) {
                        listBus.push(bus);
                        $("#area_bus").html(data);
                        $("#bus").select2('val','');
                    });
                }else{
                    swal({
                        title: "Opps !",   
                        text: "<b>Data bus/tgl berangkat/tgl kepulangan masih kosong</b>",   
                        type: "error", 
                        html: true
                    });
                }
            });
        });
    </script>
@endsection
