@if($var['check-bus'] != '0')
    <div class="alert alert-danger alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> Bus pada tanggal itu sedang dipesan</div>
@endif
<!-- /row -->
<div class="row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table color-table inverse-table table-hover">
                <thead>
                    <tr>
                        <th width="30px" style="text-align: center;"><b>Hapus</b></th>
                        <th style="text-align: center;"><b>Kode</b></th>
                        <th style="text-align: center;"><b>Plat Nomor</b></th>
                        <th style="text-align: center;"><b>Kode Warna</b></th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $no=0;
                    @endphp
                    @foreach($listPemesananBus as $item)     
                        <tr>
                            <td class="text-center">
                                @if($var['method']=='show')
                                    <a class="btn btn-danger btn-xs disabled"><i class="fa fa-trash-o"></i></a>
                                @else
                                    <a class="btn btn-danger btn-xs" onClick="hapusPemesananBus('{{ $no }}')"><i class="fa fa-trash-o"></i></a>
                                @endif
                            </td>
                            <td style="text-align: center;">{{ $item['kode'] }}</td>
                            <td style="text-align: center;">{{ $item['plat_nomor'] }}</td>
                            <td style="background-color:{{ $item['warna'] }}">&nbsp;</td>
                        </tr>
                        @php
                            $no++;
                        @endphp
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- /.row -->