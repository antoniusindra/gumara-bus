@extends('layout.layout')

@section('konten')
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Pemesanan</h4>
        </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
                <li><a href="#">Beranda</a></li>
                <li class="active">Pemesanan</li>
            </ol>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="white-box">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="active nav-item"><a href="{{ url('/pemesanan') }}"><span class="visible-xs"><b>Lihat Data</b></span><span class="hidden-xs"><b>Lihat Data</b></span></a></li>
                    <li class="nav-item"><a href="{{ url('/pemesanan/create') }}"><span class="visible-xs"><b>Input Data</b></span> <span class="hidden-xs"><b>Input Data</b></span></a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active">
                        <div class="col-md-6">
                            <a href="{{ url('/pemesanan/create') }}" class="btn btn-primary">Tambah Data</a>
                        </div>
                        <div class="col-md-6">
                            <form method="GET" action="">
                                <div class="input-group m-bot15">
                                    <input name="cari" type="text" class="form-control" placeholder="Inputkan Pencarian">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="submit">Cari</button>
                                    </span>
                                </div>
                            </form>
                            <br />
                        </div>
                        <div class="col-md-12"> 
                            <legend></legend>
                        </div>

                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table color-table inverse-table table-hover">
                                    <thead>
                                        <tr>
                                            <th width="130px" style="text-align: center;"><b>Aksi</b></th>
                                            <th style="text-align: center;"><b>No. Pemesanan</b></th>
                                            <th style="text-align: center;"><b>Nama</b></th>
                                            <th style="text-align: center;"><b>Telepon</b></th>
                                            <th style="text-align: center;"><b>Tgl Berangkat</b></th>
                                            <th style="text-align: center;"><b>Tgl Kepulangan</b></th>
                                            <th style="text-align: center;"><b>Tujuan</b></th>
                                            <th style="text-align: center;"><b>Status</b></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                            $no = 0;
                                        @endphp
                                        @foreach($listPemesanan as $item)
                                            @php
                                                $no++;
                                            @endphp
                                            <tr>
                                                <td class="text-center">
                                                    {!! Form::open(['method'=>'DELETE', 'route'=> ['pemesanan.destroy', $item->id.$var['url']['all']], 'class'=> 'delete_form']) !!}
                                                    {!! Form::hidden('nomor', $no, ['class'=>'form-control']) !!}
                                                    <div class="btn-group" style="width: 130px;">
                                                        <button type="submit" class="btn btn-danger hapusPemesanan btn-sm"><i class="fa fa-trash-o"></i></button>
                                                        <a href="{{ url('/pemesanan/'.$item->id.'/edit'.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
                                                        <a href="{{ url('/pemesanan/'.$item->id.$var['url']['all'])}}" class="btn btn-primary btn-sm"><i class="fa fa-search"></i></a>
                                                        <a href="{{ url('/pemesanan/cetak/'.$item->id)}}" class="btn btn-primary btn-sm" target="_blank"><i class="fa fa-print"></i></a>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </td>
                                                <td>{{ $item['no_pemesanan'] }}</td>
                                                <td>{{ $item['nama'] }}</td>
                                                <td>{{ $item['telepon'] }}</td>
                                                <td>{{ $item['tanggal_keberangkatan'] }}</td>
                                                <td>{{ $item['tanggal_kepulangan'] }}</td>
                                                <td>{{ $item['tujuan'] }}</td>
                                                <td>{{ $item['status'] }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-md-6 offset-md-6">
                             {{ $listPemesanan->render() }}
                        </div>
              
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $().ready(function() {   
            //Confirm Message
            $('button.btn-danger.hapusPemesanan').on('click', function(e){
                e.preventDefault();
                var $self = $(this);
                swal({   
                    reverseButton: true,
                    title: "Data yakin dihapus ?",   
                    text: "<b>Mohon diteliti sebelum menghapus data</b><br><b>Data pelunasan dan pengeluaran akan ikut terhapus</b>",   
                    type: "warning",  
                    confirmButtonText: "Hapus",   
                    cancelButtonText: "Batal",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",    
                    closeOnConfirm: false,
                    html: true
                }, function(){ 
                    $self.parents(".delete_form").submit();
                });
            });
        });
    </script>
@endsection