<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//LOGIN
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login-post');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('beranda/kalender-1', 'BerandaController@getKalender');
Route::get('beranda', 'BerandaController@index')->name('beranda');

Route::post('bus/cek-validasi', 'BusController@cekValidasi');
Route::resource('bus', 'BusController');

Route::post('karyawan/cek-validasi', 'KaryawanController@cekValidasi');
Route::resource('karyawan', 'KaryawanController');

Route::get('pemesanan/cetak/{id}', 'PemesananController@cetakPemesanan');
Route::get('pemesanan/bus2', 'PemesananController@getListBus2');
Route::post('pemesanan/hapus-bus', 'PemesananController@deleteBus');
Route::post('pemesanan/bus', 'PemesananController@getListBus');
Route::resource('pemesanan', 'PemesananController');

Route::get('pelunasan/cetak/{id}', 'PelunasanController@cetakSuratJalan');
Route::post('pelunasan/cetak', 'PelunasanController@cetakSuratJalan2')->name('pelunasan.cetak-surat-jalan');
Route::get('pelunasan/bus2', 'PelunasanController@getListBus2');
Route::post('pelunasan/hapus-bus', 'PelunasanController@deleteBus');
Route::post('pelunasan/bus', 'PelunasanController@getListBus');
Route::post('pelunasan/list-pemesanan', 'PelunasanController@getListPemesanan');
Route::get('pelunasan/list-pemesanan-2', 'PelunasanController@listPemesanan2');
Route::get('pelunasan/list-pemesanan', 'PelunasanController@listPemesanan');
Route::resource('pelunasan', 'PelunasanController');

Route::get('pengeluaran/cetak/{id}', 'PengeluaranController@cetakPengeluaran');
Route::get('pengeluaran/biaya-rincian2', 'PengeluaranController@getListBiayaRincian2');
Route::post('pengeluaran/hapus-biaya-rincian', 'PengeluaranController@deleteBiayaRincian');
Route::post('pengeluaran/biaya-rincian', 'PengeluaranController@getListBiayaRincian');
Route::get('pengeluaran/bus2', 'PengeluaranController@getListBus2');
Route::post('pengeluaran/list-pemesanan', 'PengeluaranController@getListPemesanan');
Route::get('pengeluaran/list-pemesanan', 'PengeluaranController@listPemesanan');
Route::resource('pengeluaran', 'PengeluaranController');

Route::post('laporan/laporan-pemasukan', 'LaporanController@laporan')->name('laporan.cetak-pemasukan');
Route::post('laporan/jadwal-bus', 'LaporanController@cetakJadwalBus')->name('laporan.jadwal-bus');
Route::resource('laporan', 'LaporanController');

Route::post('user/cek-validasi', 'UserController@cekValidasi');
Route::resource('user', 'UserController');
// Route::resource('profil-perusahaan', 'ProfilPerusahaanController');
Route::resource('presentase', 'PresentaseController');

Route::get('info-pengguna', 'LainLainController@infoPengguna');
Route::post('ganti-password', 'LainLainController@updatePassword')->name('ganti-password');
Route::get('ganti-password', 'LainLainController@formGantiPassword');

